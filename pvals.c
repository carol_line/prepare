/*****************************************************************************/
/*                           Driver parsovani textu                          */
/*                   (c) 2016 ELITRONIC s.r.o. by K.L.soft                   */
/*                  aparse.c. Zapocato 13.06.2016, 04.03.21                  ./
-------------------------------------------------------------------------------
Pripominky: 
===========     
22.04.22
Otocit hierartchii. PVALS_nsize_e sizenumber definovat zde a prenaset to do
prepare a aparse.
          
12,03,21
Napadlo me, ze lze resit ruzne typy pomoci unionu.
val.byte
val.word
val.dword
val.float

*******************************************************************************/
/*----------------------------------------------------------------------------*/
// Includes                                                                    
/*----------------------------------------------------------------------------*/
#include "pvals.h"
#include "config.h"

/*----------------------------------------------------------------------------*/
// Definice konstatnt                                                         
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
// Definice promennych                                                        
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
// Definice funkci                                                      
/*----------------------------------------------------------------------------*/



/*******************************************************************************
 *Soucasne ukladaji i ukazatel na zacatek kazdeho udaje.
 */
void pvals_main_init(pvals_main_t * pself)
{
  pself->DataIdx = 0;
  pself->ItemCnt = 0;      
}

/*******************************************************************************
 *Soucasne ukladaji i ukazatel na zacatek kazdeho udaje.
 */
void pvals_save_sysrule(pvals_main_t * pself, pvals_type_e pvtype, unsigned short pvact, unsigned short pvlevel)
{
  pself->ItemTab[pself->ItemCnt].Type = pvtype;
  pself->ItemTab[pself->ItemCnt].ActIdx = pvact;
  pself->ItemTab[pself->ItemCnt].Level = pvlevel;
  pself->ItemTab[pself->ItemCnt].BeginIdx = pself->DataIdx;
  
  pself->ItemCnt++;
} 

/*******************************************************************************
 *Soucasne ukladaji i ukazatel na zacatek kazdeho udaje.
 */
void pvals_save_pbracket(pvals_main_t * pself, unsigned short pvact, unsigned short pvlevel, unsigned short pvok)
{
  pvals_alltypes_u tempv;

  pvals_save_sysrule(pself, PVALS_TYPE_PBRACKET, pvact, pvlevel);
    
  tempv.Val16 = (int16_t)pvok;

  pself->DataBuf[pself->DataIdx] = tempv.Array[0]; pself->DataIdx++;
  pself->DataBuf[pself->DataIdx] = tempv.Array[1]; pself->DataIdx++;
} 
 
/*******************************************************************************
 *Funkce zaradi polozku do bufferu parsovanych hodnot.
 *  Je to udelane podle knihovny Ascanf, kde se rozlisuje typ a podle toho
 *  se nastavi promenna. Funkce Ascanf se krmi znaky a sama vrati status,
 *  jestli je cislo hotovo nebo jestli je chyba.
 */
void pvals_save_numeric(pvals_main_t * pself, pvals_type_e pvtype, unsigned short pvact, unsigned short pvlevel)
{
  pvals_alltypes_u tempv;

  pvals_save_sysrule(pself, pvtype, pvact, pvlevel);

  switch (pvtype) {
    case PVALS_TYPE_BYTE: // 8 bit
      pself->DataBuf[pself->DataIdx] = (int8_t)pself->ScanfSession.Work.Val32; pself->DataIdx++;
    break;

    case PVALS_TYPE_WORD: // 16 bit
      tempv.Val16 = (int16_t)pself->ScanfSession.Work.Val32;

      pself->DataBuf[pself->DataIdx] = tempv.Array[0]; pself->DataIdx++;
      pself->DataBuf[pself->DataIdx] = tempv.Array[1]; pself->DataIdx++;
    break;

    case PVALS_TYPE_DWORD: // 32 bit
      tempv.Val32 = pself->ScanfSession.Work.Val32;

      pself->DataBuf[pself->DataIdx++] = tempv.Array[0];
      pself->DataBuf[pself->DataIdx++] = tempv.Array[1];
      pself->DataBuf[pself->DataIdx++] = tempv.Array[2];
      pself->DataBuf[pself->DataIdx++] = tempv.Array[3];
    break;

    case PVALS_TYPE_FLOAT: // float bit
      tempv.ValFP = pself->ScanfSession.Work.ValFP;

      pself->DataBuf[pself->DataIdx++] = tempv.Array[0];
      pself->DataBuf[pself->DataIdx++] = tempv.Array[1];
      pself->DataBuf[pself->DataIdx++] = tempv.Array[2];
      pself->DataBuf[pself->DataIdx++] = tempv.Array[3];
    break;  
    
    case PVALS_TYPE_STRING:
      // Uklada se jinak
    break;   
    
    default:
      // Ostatni se taky ukladaji jinak
    break;
  }
}

/*******************************************************************************
 *Funkce zaradi do bufferu parsovanych hodnot text.
 *
 */
void pvals_save_string_open(pvals_main_t * pself, unsigned short pvact, unsigned short pvlevel, char znak)
{
  #ifdef __ALLOW_DEBUG_DUMP
    printf(" PVALS BEG Cnt:%d Idx:%d %c ",pself->ItemCnt, pself->DataIdx, znak);
  #endif

  pvals_save_sysrule(pself, PVALS_TYPE_STRING, pvact, pvlevel);

  pself->DataBuf[pself->DataIdx++] = znak;    
}

/*******************************************************************************
 *Funkce zaradi do bufferu parsovanych hodnot text.
 *
 */
void pvals_save_string_store(pvals_main_t * pself, char znak)
{
  pself->DataBuf[pself->DataIdx++] = znak;    
}

/*******************************************************************************
 *Funkce zaradi do bufferu parsovanych hodnot text.
 *
 */
void pvals_save_string_close(pvals_main_t * pself)
{
  #ifdef __ALLOW_DEBUG_DUMP
    printf(" PVALS END Cnt:%d Idx:%d ",pself->ItemCnt, pself->DataIdx);
  #endif
  
  pself->DataBuf[pself->DataIdx++] = '\0';   
}

/*******************************************************************************
 *
 *
 *
 */
unsigned short pvals_get_okbranch(pvals_main_t * pself, unsigned short valsord)
{
  pvals_alltypes_u tempv;
  unsigned short valsidx;

  valsidx = pself->ItemTab[valsord].BeginIdx;

  tempv.Array[0] = pself->DataBuf[valsidx++];
  tempv.Array[1] = pself->DataBuf[valsidx++];

  return ((unsigned short)tempv.Val16);
}

/*******************************************************************************
 *Funkce zaradi polozku do bufferu parsovanych hodnot.
 *  Je to udelane podle knihovny Ascanf, kde se rozlisuje typ a podle toho
 *  se nastavi promenna. Funkce Ascanf se krmi znaky a sama vrati status,
 *  jestli je cislo hotovo nebo jestli je chyba.
 *
 *
 */
int8_t pvals_get_int8(pvals_main_t * pself, unsigned short valsord)
{
  unsigned short valsidx;

  valsidx = pself->ItemTab[valsord].BeginIdx;
  return (pself->DataBuf[valsidx]);
}

/*******************************************************************************
 *
 *
 */
int16_t pvals_get_int16(pvals_main_t * pself, unsigned short valsord)
{
  pvals_alltypes_u tempv;
  unsigned short valsidx;

  valsidx = pself->ItemTab[valsord].BeginIdx;

  tempv.Array[0] = pself->DataBuf[valsidx++];
  tempv.Array[1] = pself->DataBuf[valsidx++];

  return (tempv.Val16);
}

/*******************************************************************************
 *
 *
 */
int32_t pvals_get_int32(pvals_main_t * pself, unsigned short valsord)
{
  pvals_alltypes_u tempv;
  unsigned short valsidx;

  valsidx = pself->ItemTab[valsord].BeginIdx;

  tempv.Array[0] = pself->DataBuf[valsidx++];
  tempv.Array[1] = pself->DataBuf[valsidx++];
  tempv.Array[2] = pself->DataBuf[valsidx++];
  tempv.Array[3] = pself->DataBuf[valsidx++];

  return (tempv.Val32);
}

/*******************************************************************************
 *
 *
 */
float pvals_get_float(pvals_main_t * pself, unsigned short valsord)
{
  pvals_alltypes_u tempv;
  unsigned short valsidx;

  valsidx = pself->ItemTab[valsord].BeginIdx;

  tempv.Array[0] = pself->DataBuf[valsidx++];
  tempv.Array[1] = pself->DataBuf[valsidx++];
  tempv.Array[2] = pself->DataBuf[valsidx++];
  tempv.Array[3] = pself->DataBuf[valsidx++];

  return (tempv.ValFP);
}

/*******************************************************************************
 *
 *
 */
void pvals_get_string(pvals_main_t * pself, unsigned short valsord, char * ptext)
{
  unsigned short i;
  unsigned short valsidx;

  valsidx = pself->ItemTab[valsord].BeginIdx;

  for (i = 0; i < PVALS_LNG_DATABUF; i++) {
    *ptext = pself->DataBuf[valsidx];
    if (*ptext == '\0') {
      return;
    }
    else {
      ptext++;
      valsidx++;
    }
  }
  pself->Error |= PVALS_ERR_GETSTRING;
}

/******************************************************************************/
/******************************************************************************/
