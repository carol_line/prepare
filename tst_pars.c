/**************************************************************************************/
/*                           Testovaci program parseru APARSE                         */
/*                                   (c) 2021 K.L.soft                                */
/*                             TST_PARS.C. Zapocato 07.03.21                          */
/*************************************************************************************
05.05.22
Toto je varianta testovaciho programu pro macro1, test spousteni maker primo pomoci
funkce: void aparse_call_macro(aparse_main_t * pself, char macro_id)

Makro skonci script errorem FINDOR a ten nijak neobnovi auxbuf. Takze auxbuf obnovit po
neuspechu rucne. Konec koncu ulozenej by mel byt. uklada se v next_rule.


*/
/*====================================================================================*/
// USED PARSING SCRIPT
/*====================================================================================*/

#define JOIN_(X,Y) X##Y
#define JOIN(X,Y) JOIN_(X,Y)


#define TST_PARS_SCRIPT_FILE SCRJSON


/*====================================================================================*/
// INCLUDES
/*====================================================================================*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "pvals.h"
#include "scrjson.h"
#include "aparse.h"

/*====================================================================================*/
// FUNCTION DEFINITIONS
/*====================================================================================*/


aparse_main_t
  Main_Parser1;

const char
  * Main_ErrTextTab[] = {
  "SCRIPT_NOERROR   ",
  "SCRIPT_GETCHAR   ",
  "SCRIPT_SYMBOLSCNT",
  "SCRIPT_DATACNT   ",
  "SCRIPT_LEVELINC  ",
  "SCRIPT_LEVELDEC  ",
  "SCRIPT_NEXTRULE  ",
  "SCRIPT_SYNCTEXT  ",
  "SCRIPT_PBRACKET  ",
  "SCRIPT_PREPEAT   ",
  "SCRIPT_FINDOR    ",
  "SCRIPT_FINDEND   ",
  "SCRIPT_DTSTATIC  ",
  "SCRIPT_DTDEFAULT ",
  "SCRIPT_SCDEFAULT ",
  "SCRIPT_LAST      ",
  "DATA_NOERROR     ",
  "DATA_SPACE       ",
  "DATA_STATIC1     ",
  "DATA_STATIC2     ",
  "DATA_VARTEXTD1   ",
  "DATA_VARTEXTT1   ",
  "DATA_SYNCTEXT    ",
  "DATA_VARNUM1     ",
  "DATA_VARNUM2     ",
  "DATA_VARNUM3     "
};


/***************************************************************************************
 *Funkce

   ASCANF_NFORMAT_UNSIGNED  = 'u',
   ASCANF_NFORMAT_SIGNED    = 'd',
   ASCANF_NFORMAT_MUMERIC   = 'n',
   ASCANF_NFORMAT_FLOAT     = 'f',
   ASCANF_NFORMAT_EXPONENT  = 'e',
   ASCANF_NFORMAT_EXPFLOAT  = 'g',
   ASCANF_NFORMAT_HEXADECM  = 'x'

   PVALS_TYPE_BYTE      = 'b',
   PVALS_TYPE_WORD      = 'w',
   PVALS_TYPE_DWORD     = 'd',
   PVALS_TYPE_FLOAT     = 'f',
   PVALS_TYPE_STRING    = 's'


 */
void tst_pars_displval(aparse_main_t * pparser)
{
  pvals_alltypes_u tempv;
  char retez[128];
  unsigned short i, okbranch, pvord, pvlevel;
  char pvsymbol;      
        
  printf(" Count items:%u Level:%u\r\n", pparser->PVals.ItemCnt, pparser->Level.ActIdx);
  printf("------------------------------------\r\n");

  for (i = 0; i < pparser->PVals.ItemCnt; i++) {
    pvord = pparser->OrdersPtr[pparser->PVals.ItemTab[i].ActIdx];
    pvlevel = pparser->PVals.ItemTab[i].Level;
    
    switch (pparser->PVals.ItemTab[i].Type) {
      case PVALS_TYPE_BYTE   :  // = 'b',
        tempv.Val8 = pvals_get_int8(&pparser->PVals, i);
        printf(" Par:%02d  Type: %c Ord:%02d, Level:%02d Value:%d Float:%f \r\n", i, pparser->PVals.ItemTab[i].Type, pvord, pvlevel, tempv.Val8, tempv.ValFP);
      break;

      case PVALS_TYPE_WORD   :  // = 'w',
        tempv.Val16 = pvals_get_int16(&pparser->PVals, i);
        printf(" Par:%02d  Type: %c Ord:%02d, Level:%02d Value:%d Float:%f \r\n", i, pparser->PVals.ItemTab[i].Type, pvord, pvlevel, tempv.Val16, tempv.ValFP);
      break;

      case PVALS_TYPE_DWORD  :  // = 'd',
        tempv.Val32 = pvals_get_int32(&pparser->PVals, i);
        printf(" Par:%02d  Type: %c Ord:%02d, Level:%02d Value:%d Float:%f \r\n", i, pparser->PVals.ItemTab[i].Type, pvord, pvlevel, tempv.Val32, tempv.ValFP);
      break;

      case PVALS_TYPE_FLOAT  :  // = 'f',
        tempv.ValFP = pvals_get_float(&pparser->PVals, i);
        printf(" Par:%02d  Type: %c Ord:%02d, Level:%02d Value:%f \r\n", i, pparser->PVals.ItemTab[i].Type, pvord, pvlevel, tempv.ValFP);
      break;

      case PVALS_TYPE_STRING :  // = 's'
        pvals_get_string(&pparser->PVals, i, retez);
        printf(" Par:%02d  Type: %c Ord:%02d, Level:%02d Value:%s \r\n", i, pparser->PVals.ItemTab[i].Type, pvord, pvlevel, retez);
      break;

      case PVALS_TYPE_SYSRULE:
        pvsymbol = (char)pparser->SymbolsPtr[pparser->PVals.ItemTab[i].ActIdx];
        printf(" Par:%02d  Rule:%c%c Ord:%02d, Level:%02d \r\n", i, pparser->PVals.ItemTab[i].Type, pvsymbol, pvord, pvlevel);
      break;

      case PVALS_TYPE_PBRACKET:
        pvsymbol = (char)pparser->SymbolsPtr[pparser->PVals.ItemTab[i].ActIdx];
        okbranch = pvals_get_okbranch(&pparser->PVals, i);
        printf(" Par:%02d  Rule:%c%c Ord:%02d, Level:%02d OKbranch:%d \r\n", i, pparser->PVals.ItemTab[i].Type, pvsymbol, pvord, pvlevel, okbranch);
      break;
    }
  }
}

/***************************************************************************************
 *Funkce
 */
void tst_pars_displrules(aparse_main_t * pparser)
{
  int j;
  prepare_structptr_u struct_ptr;
  char * textptr;

  printf("\r\nParsing rules with additional parameters:\r\n");
  printf("-----------------------------------------\r\n");
  printf("Id  Ord Idx R Re Sca\r\n");
  printf("-----------------------------------------\r\n");

  for (j = 0; j < pparser->RuleCnt; j++) {

    struct_ptr.void_ptr = (void *)(pparser->DataPtr + pparser->IndexesPtr[j] - 1);

    switch (Main_Parser1.SymbolsPtr[j]) {
      case PREPARE_SYMBOL_ANYSPACE      :  // '-'
        printf("%03d %03d %03d %c  %c %03d - \r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.anyspace_ptr->Reverser, struct_ptr.anyspace_ptr->ChrScale);
      break;
      case PREPARE_SYMBOL_ONEORMORESPACE:  // '='
        printf("%03d %03d %03d %c  %c %03d - \r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.oneormorespace_ptr->Reverser, struct_ptr.oneormorespace_ptr->ChrScale);
      break;
      case PREPARE_SYMBOL_STATICTEXT    :  // '%'
        textptr = (char *)pparser->DataPtr + struct_ptr.statictext_ptr->TextIdx;
        printf("%03d %03d %03d %c  %c     - %c, %d, %s\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.statictext_ptr->Reverser,
        struct_ptr.statictext_ptr->Type, struct_ptr.statictext_ptr->TextIdx, textptr);
      break;
      case PREPARE_SYMBOL_VARIABLETEXT  :  // '$'
        printf("%03d %03d %03d %c  %c %03d - %c, %c\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.variabletext_ptr->Reverser, struct_ptr.variabletext_ptr->ChrScale,
        struct_ptr.variabletext_ptr->Type, struct_ptr.variabletext_ptr->Separator);
      break;
      case PREPARE_SYMBOL_SYNCTEXT     :  // '?'
        textptr = (char *)pparser->DataPtr + struct_ptr.synctext_ptr->TextIdx;
        if (struct_ptr.synctext_ptr->Type == PREPARE_TXSTATIC_NORMAL) {
          printf("%03d %03d %03d %c  %c %03d - Norm:%c, %d, %s\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.synctext_ptr->Reverser, struct_ptr.synctext_ptr->ChrScale,
          struct_ptr.synctext_ptr->Type, struct_ptr.synctext_ptr->TextIdx, textptr);
        }
        if (struct_ptr.synctext_ptr->Type == PREPARE_TXSTATIC_WHITE) {
          printf("%03d %03d %03d %c  %c %03d - White:%c, %d, %d\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.synctext_ptr->Reverser, struct_ptr.synctext_ptr->ChrScale,
          struct_ptr.synctext_ptr->Type, struct_ptr.synctext_ptr->TextIdx, (int)*textptr);
        }
      break;
      case PREPARE_SYMBOL_VARIABLENUMBER:  // '#'
        printf("%03d %03d %03d %c  %c %03d - %c, %c\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.variablenumber_ptr->Reverser, struct_ptr.variablenumber_ptr->ChrScale,
        struct_ptr.variablenumber_ptr->Type, struct_ptr.variablenumber_ptr->Format);
      break;
      case PREPARE_SYMBOL_SKIPTEXT      :  // '+'
        printf("%03d %03d %03d %c  %c %03d - type:%c \r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.skiptext_ptr->Reverser, struct_ptr.skiptext_ptr->ChrScale, (char)struct_ptr.skiptext_ptr->Type);
      break;
      case PREPARE_SYMBOL_OR            :  // '|'
        printf("%03d %03d %03d %c  \r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j]);
      break;
      case PREPARE_SYMBOL_AND           :  // ','
        printf("%03d %03d %03d %c  \r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j]);
      break;
      case PREPARE_SYMBOL_ENDBLOCK      :  // ';'
        printf("%03d %03d %03d %c        - %d\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.endblock_ptr->RetIdx);
      break;
      case PREPARE_SYMBOL_INITVALS      :  // '&'
        printf("%03d %03d %03d %c\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j]);
      break;
      case PREPARE_SYMBOL_CALLTASK      :  // '!'
        printf("%03d %03d %03d %c        - %d\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.calltask_ptr->Number);
      break;
      case PREPARE_SYMBOL_MACRO         :  // '*'
        printf("%03d %03d %03d %c        - %d, %d\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.macro_ptr->Id, struct_ptr.macro_ptr->BeginIdx);
      break;
      case PREPARE_SYMBOL_LBRACKET      :  // '('
      case PREPARE_SYMBOL_LOPTION       :  // '['
      case PREPARE_SYMBOL_LREPEAT       :  // '{'
        printf("%03d %03d %03d %c        - %d\r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j], struct_ptr.lbracket_ptr->BeginIdx);
      break;
      case PREPARE_SYMBOL_PBRACKET      :  // ')'
      case PREPARE_SYMBOL_POPTION       :  // ']'
      case PREPARE_SYMBOL_PREPEAT       :  // '}'
        printf("%03d %03d %03d %c        - \r\n", j, pparser->OrdersPtr[j], pparser->IndexesPtr[j], pparser->SymbolsPtr[j]);
      break;
      default:
        printf("Unknown symbol\r\n");
      break;
    }
  }
}

/***************************************************************************************
 *Funkce
 */
void tst_pars_callback(aparse_main_t * pparser, aparse_callback_e task)
{
 // unsigned short order, actidx, nextidx, okbranch;
 // char znaknaact;
 // aparse_error_e error;

//  order = aparse_get_order(pself);
//  actidx = pself->RuleActIdx;
//  nextidx = pself->RuleNextIdx;
//  okbranch = aparse_get_okbranch(pself);
//  znaknaact = pself->AuxBuf.Data[pself->AuxBuf.RsIdx];
//  error = pself->Error.Type;

  switch (task) {
    case APARSE_CALLBACK_CODE_REPEAT:
  //    printf("\r\n*** Callback function Task REPEAT: Ord:%d Act:%d, Next:%d OK:%d inch:%c Err:%s", order, actidx, nextidx, okbranch, znaknaact, Main_ErrTextTab[error]);
      printf("\r\nKonec repeatu");
    break;

    case APARSE_CALLBACK_CODE_MACRO:
 //     printf("\r\n*** Callback function Task MACRO: Ord:%d Act:%d, Next:%d OK:%d inch:%c Err:%s", order, actidx, nextidx, okbranch, znaknaact, Main_ErrTextTab[error]);
      printf("\r\nKonec makra");
    break;

    case APARSE_CALLBACK_CODE_BRACKET:
 //     printf("\r\n*** Callback function Task BRACKET:Ord:%d Act:%d, Next:%d OK:%d inch:%c Err:%s", order, actidx, nextidx, okbranch, znaknaact, Main_ErrTextTab[error]);
      printf("\r\nKonec bloku");
    break;

    case APARSE_CALLBACK_CODE_OPTION:
 //     printf("\r\n*** Callback function Task OPTION: Ord:%d Act:%d, Next:%d OK:%d inch:%c Err:%s", order, actidx, nextidx, okbranch, znaknaact, Main_ErrTextTab[error]);
      printf("\r\nKonec optionu");
    break;

    case APARSE_CALLBACK_CODE_ERROR:
 //     printf("\r\n*** Callback function Task ERROR: Ord:%d Act:%d, Next:%d OK:%d inch:%c Err:%s", order, actidx, nextidx, okbranch, znaknaact, Main_ErrTextTab[error]);
    break;

    case APARSE_CALLBACK_CODE_SCALE:
 //     printf("\r\n*** Callback function Task SCALE: Ord:%d Act:%d, Next:%d OK:%d inch:%c Err:%s", order, actidx, nextidx, okbranch, znaknaact, Main_ErrTextTab[error]);
    break;

    case APARSE_CALLBACK_CODE_PVALS:
      printf("\r\nZaplnen buffer PVALS. Level:%u", pparser->Level.ActIdx);
      tst_pars_displval(pparser); 
      pvals_main_init(&pparser->PVals);
    break;

    case APARSE_CALLBACK_CODE_SCRIPT:

      switch (pparser->Number) {
        case 1:
          printf("\r\nByl uplny konec. Level:%u\r\nPredchozi hodnoty:", pparser->Level.ActIdx);
          tst_pars_displval(pparser);
        break;
        case 2:
          printf("\r\nByl nazev klice. Level:%u", pparser->Level.ActIdx);
        break;
        case 3:
          printf("\r\nZacatek nove hodnoty. Level:%u\r\nPredchozi hodnoty:", pparser->Level.ActIdx);
          tst_pars_displval(pparser);
        break;
        case 4:
          printf("\r\nByl zacatek pole. Level:%u", pparser->Level.ActIdx);
        break;
      }
    break;

  }
}
       
/***************************************************************************************
 *Funkce
 */
int main(int argc, char * argv[])
{
  int main_state, read_len, data_len, err_status, i;
  FILE * data_stream;
  prepare_status_e pstatus;
  char data_store[10000];
  char * data_ptr;

  main_state = 0;

  for (i = 0; i < 60000; i++) {
    switch (main_state) {
      case 0:
        printf("\r\nLoading data file...");

        if (argv[1] == NULL) {
          printf("Usage: tst_pars <filename>. Program unexpectedly terminated.\r\n");
          return (0);
        }
        read_len = strlen(argv[1]);
        if (read_len > 12) {
          printf("Input file error01. Program unexpectedly terminated.\r\n");
          return (0);
        }
        data_stream = fopen(argv[1], "r");
        if (data_stream == NULL) {
          printf("Input file error02. Program unexpectedly terminated.\r\n");
          return (0);
        }
        err_status = fseek(data_stream, 0, SEEK_END);
        if (err_status != 0) {
          printf("Input file error03. Program unexpectedly terminated.\r\n");
          fclose(data_stream);
          return (0);
        }
        data_len = ftell(data_stream);
        if (data_len == -1) {
          printf("Input file error04. Program unexpectedly terminated.\r\n");
          fclose(data_stream);
          return (0);
        }
        err_status = fseek(data_stream, 0, SEEK_SET);
        if (err_status != 0) {
          printf("Input file error05. Program unexpectedly terminated.\r\n");
          fclose(data_stream);
          return (0);
        }
        read_len = fread(data_store, 1, data_len, data_stream);
        if (read_len < 1) {
          printf("Input file error06. Program unexpectedly terminated.\r\n");
          fclose(data_stream);
          return (0);
        }

        data_store[read_len] = '\0';
        printf("Loaded:%d bytes.\r\n", read_len);
        printf("\r\nParsed data file content:\r\n");
        printf("-------------------------\r\n");
        printf("%s\r\n", data_store);

        fclose(data_stream);
        main_state = 1;
      break;

      case 1:
        pstatus = aparse_main_init(&Main_Parser1,
        JOIN(TST_PARS_SCRIPT_FILE,_Symbols), JOIN(TST_PARS_SCRIPT_FILE,_Orders), JOIN(TST_PARS_SCRIPT_FILE,_Indexes), JOIN(TST_PARS_SCRIPT_FILE,_Data), JOIN(TST_PARS_SCRIPT_FILE,_CODECNT), JOIN(TST_PARS_SCRIPT_FILE,_DATACNT));

        switch (pstatus) {
          case PREPARE_STATUS_OK:  printf("\r\nData file loaded successfuly.\r\n"); main_state = 2; break;
          case PREPARE_STATUS_ERR: printf("\r\nLoading failed, error code %d\r\n", Main_Parser1.Error.Type); main_state = 5; break;
        }
      break;

      case 2:
        tst_pars_displrules(&Main_Parser1);
        printf("\r\nResults of parsing:\r\n");
        printf("-----------------------------------------\r\n");
        printf("   st act rul ord\r\n");
        printf("-----------------------------------------\r\n");

        data_ptr = data_store;
        Main_Parser1.CallBackPtr = &tst_pars_callback;
        aparse_auxbuf_init(&Main_Parser1);
        aparse_start_processing(&Main_Parser1);

        main_state = 3;
      break;

      case 3:
        if (*data_ptr != '\0') {
          if (*data_ptr == '\n') {
            data_ptr++;
          }
          else {
            if (aparse_auxbuf_ready(&Main_Parser1)) {
              aparse_auxbuf_putchar(&Main_Parser1, *data_ptr++);
            }
          }
        }
        aparse_script_processing(&Main_Parser1);
        aparse_data_processing(&Main_Parser1);

        switch (Main_Parser1.ScriptState) {
          case APARSE_SCRIPT_STATE_SCRIPTERR:  printf("\r\nScript error, program terminated.\r\n"); main_state = 5; break;
          case APARSE_SCRIPT_STATE_DATAERR:    printf("\r\nData error, program terminated.\r\n"); main_state = 5; break;
          case APARSE_SCRIPT_STATE_ENDPARSEOK: printf("\r\nData OK, program terminated.\r\n"); main_state = 4;  break;
          default:
          break;
        }
      break;

      case 4:
        printf("\r\n END 4 \r\n");
        return (0);
      break;


      case 5:
        printf("\r\n END 5, RdIdx:%u, znak:%c \r\n", Main_Parser1.AuxBuf.RdIdx, Main_Parser1.AuxBuf.Data[Main_Parser1.AuxBuf.RdIdx]);
        return (0);
      break;

    }
  }

  printf("\r\nTIMEOUT PROGRAM TERMINATION\r\n");
  return (0);
}

/**************************************************************************************/
/**************************************************************************************/