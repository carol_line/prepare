/*****************************************************************************/
/*                           Driver parsovani textu                          */
/*                   (c) 2016 ELITRONIC s.r.o. by K.L.soft                   */
/*                        prepare.c. Zapocato 25.03.2020                     ./
-------------------------------------------------------------------------------
Ulozeni do registru "xyw pro slovo, ale je nutne byt na zacatku slova. Paste je pak "xp
sloupcovy blok je CTRL-v pak vytahnout a pak SHIFT-i.
guu  : Change current line from upper to lower.
gUU  : Change current LINE from lower to upper.
guw  : Change to end of current WORD from upper to lower.
guaw : Change all of current WORD to lower.
gUw  : Change to end of current WORD from lower to upper.
gUaw : Change all of current WORD to upper.
g~~  : Invert case to entire line
g~w  : Invert case to current WORD
guG : Change to lowercase until the end of document..
y3j zkopiruje aktualni a tri radky dolu. Staci 3Y.
y3k zkopiruje aktualni a tri radky nahoru
yi} zkopiruje obsah slozene zavorky, ve kt. se nachazi
ya} totez, ale obsahuje i ty zavorky
y4l 4znaky doprava, stejne jako y4x
y5<space> funguje na ulozeni n znaku vpravo taky
:s//novy_obsah/g   Na radku prepise vyskyty novym. Na starym se musi stisknou *.
:s//novy_obsah     Totez, ale pouze nahradi prvni vyskyt
:.,+2s/stary/novy/g od kurzoru 2 radky prepise - tecka se nemusi psat
:,$s/stary/novy/g prepise retezec do konce souboru,
:,$s//novy/g prepise retezec od kurzoru do konce na kterem jsi udelal *.
ytx zkopiruje text do znaku x
V> a pak teckou opakovat. Radek textu posouvat vpravo, vlevo je to <
dG je smazani do konce souboru
viW vaW - Oznaci blok tak jak jsme to dlouho chteli, cili ohranicen jen mezerami.
yiW yiW - urcite bude taky fungovat
:vsp - rozdeli okno na dve vedle sebe
:e. - Otevre vyber souboru aktualniho adresare
:set wrap/nowrap - wrap znamena, ze radek, pokud se nevejde, se zalomi na dalsi radek
**********************************************************************************
04.10.22
Stav priznaku pro # jr nrudrzitelny. Kombinace jako df je nesmyslna, jeste horsi je bf.
To je uplny nesmysl. Je nutne to predelat tak, aby bylo jedno f a kombinace jen u celociselnych.

   ASCANF_NFORMAT_UNSIGNED  = 'u',
   ASCANF_NFORMAT_SIGNED    = 'd',
   ASCANF_NFORMAT_MUMERIC   = 'n',  numeric je to same jako signed
   ASCANF_NFORMAT_FLOAT     = 'f',
   ASCANF_NFORMAT_EXPONENT  = 'e',
   ASCANF_NFORMAT_EXPFLOAT  = 'g',
   ASCANF_NFORMAT_HEXADECM  = 'x'
 
   PVALS_NSIZE_BYTE      = 'b',
   PVALS_NSIZE_WORD      = 'w',
   PVALS_NSIZE_DWORD     = 'd',
   PVALS_NSIZE_FLOAT     = 'f',
   PVALS_NSIZE_STRING    = 's'
 
Nema ani smysl uf a df. 
Maji smysl zkratky:

ub uw ud
db dw dd
nb nw nd
xb xw xd

f        
e     samotny flag
g        


20.07.22
Nazev parsigen pro prepare
pro aparse nejakej taky zacinajici na parsi, treba parsik1a  a jako arm, cislo verze
parsik1c bude pro c51 parsik1g bude gcc, cili pro pc mohlo by byt i parsik1p
                                                                            
parsikit                              
parsikon
parsipy pro python
parsimin pro verzi bez maker a zavorek     
parsimax
parsiarm
parsic51
parsigpc


maximars
minipars
unipars
genpars prepars
c51pars
armpars
pcpars
                                                                            

06.06.22
Dalsi chyba. Umoznuje vytvorit duplicitni Id u makra.
Tuto chybu jsem opravil.

03.06.22
Zjistil jsem, ze kdyz jsou definovana makra ve scriptu , ale nejsou nikde pouzita,
tak to zadnou chybu neohlasi. To nebudu opravovat.

Stejne tak kdyz udelam ; nekde doprostred vyrazu, tak to chybu nenahlasi a to co je za ;
je mrtvy kod. Toto uz je opraveno.


02.06.22
Blby je, ze liche zavorky se projevi az uplne na konci. Chtelo by to vymyslet nejaky
porychtung na zjisteni liche leve zavorky.


           ( ( ) { ) ( (  )   (   ) )
              -       --     ---
            ----    ----    -------

           ( ( ) { ) ( (  )   (   ) )    algoritmus to vyhodnoti spatne
              -   -     --     ---
                     ----------------

Dalsi chyba - kdyz je pouzito makro v hlavnim scriptu, tak pak assigning nevyhlasi
chybu, kdyz neni definovane a skonci to na timeout.

Nova jmena txpars_gen txpars_run
Neni to 8 znaku, no

gencpars runcpars
gpycpars rpycpars

genlpars runlpars  ten by mohl bejt pro jednodussi verzi
gpylpars rpylpars  nebo se znakem s jako simple, zatimco c
                   znamena complex nebo Charles



01.06.22
V textu statictext nelze pouzivat uvozovky. Notace \ nefunguje

27.05.22
Vyresit chybu makra, ze neni testovano v systemu levelu.
Kdyz je volani makra, tak by musel projet vsechny vnoreni i toho makra
a v nem muze byt taky makro. A navic muze byt volani makra ve slozenych
zavorkach. Takze to je prakticky nemozne to testovat.
No ale hlavne zavorky jsou primo ve vyrazu parove. * neni parova, protoze
; je nekde jinde.

17.05.22
Jeste mne napadly rules.
Okamzite skonceni parsingu. Nemuze to byt ; protoze ten ma jine vlastnosti.


16.05.22

Takze nove rules:
cislovani rules
+ poskoceni dopredu
+< poskoceni dozadu
pujde jeste:
#< parsing dozadu
$<
-<
=<
?<
%<

omezeni poctu znaku
+<10 poskoci o 10 znaku
#<10 cislo max. 10 znaku
$<10 text 10 znaku, varianta raw text
-<10 mezera max. 10 znaku
=<10 alespon jedna, ale max 10 mezer
?<10 hleda patern jen 10 znaku, pak chyba
%< ten ne

takze to vsechno je treba doprogramovat.

Uplne nove rule bude +
Pak to <
a cisla


14.05.22
Kdyz uz to budes delat, udelej cislovani rules jeste pred preusporadanim, aby to
bylo po porade ve scriptu. To pak preneseme do vsech rules.

13.05.22
Takze nove rules:

TRIGER - hleda text, postavi se za nej, ale taky pred nej. Napr.  20.50 Kc. Hleda Kc
a parsuje co je pred tim. Nebo  201 kWh.  Hleda jednotky, parsuje text pred tim.
Cili bychom mohli mit neco jako zpetny parsing. Rules jako: na zacatek cisla, preskocit
mezery, na zacatek textu, o n znaku zpet. Stejne tak i opacne rules: popredu-jako je triger
ktery synchronizuje cely text - tak trigerovat na numericke nebo nenumericke znaky obecne.

20.50 Kc se da parsovat ruzne.
1. Varianta:

#ff - %"Kc"

nebo:

Triger na Kc, zpetne preskocit mezery, zpetne parsovat cislo.
U teto varianty to ma trochu jine vlastnosti. Triger vse preskoci, coz prva varianta nema.
Symbol zpetneho parsingu bude <. <- zpetne mezery


SKIP - poposkoceni, a jak vyplyva z predchoziho textu, tak skok dopredu i dozadu.
Symbol by mohl byl + ktere neni jeste pouzito. <+ by znamenalo skok dozadu. +20 nebo <+21.
Otazka je pouziti promennych ve scriptu. Kontrola, jestli nepretekl buffer.

DELKA
Promenny text omezeny delkou. Kombinace s existujicimi pravidly. Bud musi mit
i tak charakter identifikatoru nebo musi byt v uvozovkach.
Nebo nova varianta - nic z toho.
$25i identifikator o delce 25 znaku
$13n obecne se vlozi 13 znaku.

Otazka je, kdyz treba uvozovky omezi text driv, jak s tim zachazet. jestli to cislo
bude jako maximum nebo minimum a co pak.

#13ff nebo #14dn bude to same. Co delat, kdyz cislo bude kratsi. Zde to bude asi minimum.
Kdyz bude cislo pokracovat, tak se osekne. Melo by jit bud se s bufferem zastavit za cislem
nebo postoupit o prislusny pocet znaku tak jako tak.




22.04.22
Typ  prepare_nformat_e coz jsou typy udaju ascan je treba brat z ascan. Tam to ale
neni v hlavickovem souboru. Predelat.


14.04.22
Je nutne udelat dokumentaci. Zapominam jak jsem to myslel. Napr. ten synchtotext a
v  prepare_txtype_e typ normal, jak se bude ukoncovat.

07.04.22
Ted vytvarim koncepci nove regulace a zjistuju jednu vec. A sice ze v jednom programu
muze byt potreba mnoha malych kontrol syntaxe a parsovani. Znamena to mnoho malych
scriptiku rozptylenych po programu, ktere nebudou pracovat paralelne, protoze jsou
vazany na jednu consolu, cili zdroj dat, ale nemusi mit charakter uceleneho scriptu.
Proto je treba dodelat do programu fdunkci spusteni libovolneho makra, ne jen prvniho
hlavniho. Uz to v pozada1vcich nekde je.

25.03.21
Takze jsem v podstate dokoncil i aparse. Vcera jsem se rozhodl, ze z kolekce rules
vyskrtnu setoftext. Zmenim ho na rule, ktere bude hledat urcity retezec a jakmile
tento retezec projde, tak zacne dal parsovat. Nazvu ho synchrotext. (nebo paterntext)
Muzou to byt i bile znaky jako napr. CR.

14.03.21
Funkci prepare_display_codeparams() taky udelat s unionem struct hodnot.

11.03.21
Problem se setoftext. Ten soubor textu neni nijak zkontrolovan.
Melo by se to udelat.

04.03.21
Tak Navraty jsou hotovy, ale ted delam aparse a navrat je stejny pro makro a pro zavorku
u obou je strednik, neni to nijak rozlisene, meli bychom zavest typ.
U zavorky a u makra se indexy skoku lisi v nazvu promenne. Oboji je Begin.
Chtelo by to sjednotit.


27.02.21
Problem dneska je - navratove indexy.
Jednak makra - to jsme uplne poignorovali, ze nejen zavorka, ale i makro musi mit
volaci a navratovy index. U leve zavorky mame jakz takz volaci index. Navratovy index neni nikce

Je taky potreba nekam zaznamenavat indexy volani a navratu.
U zavorky je to strednik misto prave zavorky, tak zde by to taky mohl byt strednik,
ale celeho makra. Strednik primarniho scriptu by mohl mit jako index neco, co by reklo,
ze je to uplny konec.
Problem je v tom, ze zde zadny strednik pro zapsani makra neni.
Asi bude nutne vytvaret pro to rule. nebo, jako v predesla verzi, vytvorit samostatne
pole. Mohla by to byt tabulka id, volacich i navratovych indexu.
Fakt je, ze navratove indexy bychom ani nemuseli pocitat, protoze by se mohly zaznamenavat
pri behu parsingu. To neni tak tezke. Nicmene ted nam to situaci neusnadnuje, protoze pocitame
volaci index. To nelze hned, protoze ty pozice nezname.
Pozor. U makra zcela urcite nemuzeme navrat zde pocitat, proptoze makro na rozdil od zavorky
muze byt volane z ruznych mist.
Ale - a to se musi udelat - Musi se vypocitat a do vsech volani makra zadat startovni index tohoto
makra. To se ale musi udelat zvlast az po preskupeni, az budou vsechna makra vygenerovana.
Konec koncu mame polozku level. Tu ale pouzivaji i zavorky. Nejde o Level, ale o seznam maker
Na to musi byt tabulka zvlast. Musi se s makry tvorit tabulka maker a tam se pak zapisi starty.
Po preskupeni se musi script znova projet a dosadit ve vsech volanich maker startovni index.



08.02.21
Uz jsem to resil, zevnitr callbacku vyvolat spusteni makra. Je to v souvislocti
s setoftextem. Cislo textu skonci v bufferu. Zavola se callback a ten spusti dane makro.
Nebo ne a cislo textu se jen pouzije nekde v programu.

V raise_error() je problematicke nastaveni AREA state na error, kdytz pouzivas
tu funkci i jinde, napr. v make();

V analyze vyresit pocet lichych zavorek. Ted to vyhlasi chybu az na konci
souboru. ENDSCRIPT teprve kontroluje level.
Dodelat makra.
Napsat zobrazeni obsahu struktur.
Obzvlast si posvitit na skoky na makro. Pozor na ulozeni jeho Id a Indexu skoku.

V syntaxu musime rules delit na dva druhy - rules a nevim, jeste jak je nazvat,
volani callbacku, reset bufferu, cinnosti nebo tak nejak.

19.01.21
Ted se to netyka nynejsiho ukolu programovani, ale samotnych RULES. Mam
namet na zajimavou vlastnost. Slozene zavorky {} znamenaji opakovani, ve kt.
se zrejme bude zpracovavat opakovane prevadene hodnoty. Tak by bylo dobre
delat automaticky call_back vzdy po naplneni bufferu pred tim, nez buffer
pretece. Parametr by bylo nejake cislo, jeste nevim jake, aby se to odlisilo.

14.01.21
Uvedomil jsem si u maker, ze bude nutne uchovat pozici v CodeTab[] kam se skoci.
Oproti tomu Id makra je nepodstatne. Konec koncu je to tak i se zavorkami. Ale
pozor, u zavorek budeme ukladat pozici, ktera tam bude, ale u maker to nelze.
Kdyz budeme generovat volani maker, tak jejich pozice nebude jeste vytvorena.

13.01.21
Nyni bude dodelano vygenerovani pseudokodu pro interperetaci:

LevelTab a MacroTab prijdou zrusit
LevelTab neprisla zrusit, ale predelat ze struct na obycejne pole.
MacroTab asi prijde zrusit, ale MacroCnt zustane zachovano.

RuleTab bude zdrojove pole. Bude to struktura znak Rule a index do DataTab
vytvorime DataTab jako pridavna data.
CodeTab[] bude zkonvertovana tabulka.

Kazde rule, ktere ma pridavna data, bude mit typ struktura pro tato pridavna data.

Ukazovatka
Na RuleTab[] ukazuje RuleAct. Pri vytvareni.

Pri konverzi to bude:
Cteci do RuleTab[], zapisovy do DataTab[] a zapisovy do CodeTab[]

Pro dalsi levely to bude:

Cteci do CodeTab[], zapisovy taky do CodeTab[] a startovni taky do CodeTab[]
konverze se dela do stredniku. Ve vyssich levelech ale vznikne vice stredniku, tak pozor.

WriteIdx
ReadIdx
StartIdx


---------------------------------------------------------------------------

Jeste jedno rule. Propusteni n znaku. Konstrukce napr.
Cekej na ; popr. hledej text. "BOSP"
Propust 23 znaku
#2u+5
%":"
#2u+5

Horsi bude kdyz bude toto v zavorkach a bude tam nebo.
Tech 23 znaku taky vratim nebo co. To jeste neni promysleno.


25.03.20
Ted jsem oddelil aparse.c na prepare.c.

Zjistil jsem, ze jsou dalsi udaje, ktere se ziskaji tim initem.
Pocet bloku napr. Je cela struktura Block, kde to je.
Navrhuji tedy, aby vystupem prepare nebyl ani tak text, ale
primo vyplnene polozky hlavni struktury v podobe konstantnich poli.
Text scriptu nebude vubec figurovat, alespon ne ve snadno citelne podobe.
Muzeme zjistit i takove veci jako pocet maker, max. pocet levelu
a podle toho rozvrhnout pamet. Taky max pocet vals mezi resety.
Upozornovat kdyz vals se dostanou do smycky a nebude jasny jejich pocet.



Dalsi z velkych problemu
---------------------------------

SETOFTEXT
Ziskani tech pocatku a max. delky.
Kdy to delat kdyz nechceme pro parser mit uvodni analyzu.
Jestli to ma vubec cenu. Spis asi zrusime.


Zakladni pravidla pro script:

Primarni pravidlo;
*n:makro1
     :

Komentar pouze za strednikem, cili za pravidlem nebo na noven radku.
Rozdeleni pravidla pouze mezi celymi pravidly.

Uplne na zacatku je stav jako za strednikem
 Muze byt / CR LF _.  jakykoliv jiny znak zacina primarni pravidlo.
 Cili stavy jsou:

   Primarni pravidlo    PRIMARY_RULES
     - zacne v oblasti za strednikem,
     - jakmile je ; da se oblast za prim. pravidlem

   za primarnim pravidlem    MACRO_RULES
     - zde uz zustane

Pozor. To neni tak jednoduche.
 Po startu je PRIMARY_RULES. * se povazuje za RULE pred a uvnitr pravidel.
 ; ji zmeni na MACRO_RULES, takze * statrtuje makro. Ale ihned se zmeni na PRIMARY_RULES
    aby se uvnitr makra dala povazovat za rule.

 Takze MACRO_RULES je jen behem area po stredniku. Jejina kombinace po stredniku a PRIMARY
 je za zacatku scriptu, kde * je jako kazde jine rule.


Cili kdyz se to spoji, tak vznikne:
--------------------------------------

   Pred strednikem - oblast rules        SC_BEFORE_AREA
     povazuje znaky za rules. ;=prechod na obl. za strednikem, \=oblast za rozdelovnikem
     pokud je pred primarnim pravidlem, * je povazovana za rule, ne za zacatek makra

   za strednikem - oblast                SC_AFTER_AREA
     CR LF _ - nedelaji nic, \0-konec nebo chyba, *-na makro /-na koment. Ostatni znaky chyba
     pokud je pred primarnim pravidlem, * je povazovana za rule, ne za zacatek makra
                                        \0 konec vyvola chybu


   za rozdelovnikem - oblast           BS_AFTER_AREA
     CR LF _ - nedelaji nic,  /-na koment. Ostatni znaky do oblasi pred strednikem


   komentar                             CM_INSIDE_AREA
       CR LF - komentar ukoncuje


Cili poradi jak to pujde:
---------------------------

   SC_AFTER_AREA + PRIMARY_RULES     / jde na komentar,  jakykoli znak znmena prechod na SC_BEFORE_AREA
   SC_BEFORE_AREA + PRIMARY_RULES   ; meni na MACRO_RULES  \ meni na BS_AFTER_AREA
   BS_AFTER_AREA                     / na komentar   jakykoli znak SC_BEFORE_AREA


Spis jine nazvy:
--------------------------

  Jeste budou rezimy pro preskakovani textu v uvozovkach. To udelame asi najednou.
  Mozna snad jen sadu textu ne.


*******************************************************************************/

/*******************************************************************************
 *Funkce
    Problemy - zde se pocita pozice rule ve scriptu, takze ukazatel je v podstate
    dulezitejsi nez hodnota. Planuje se seznam pozic rule.
    Problem spociva pouze v tom, ze na * mame dve ulohy. To je jediny problem.
    --------------
    bude to muse byt tak, ze si to bude vyhodnocovat az primo
    funkce u sebe. Vyhybka v area nebude.
    -----------------
    Je to jeste jinak. V jednom provedeni je to rule volani makra a je to volane rule,
    ktere musi byt v seznamu a hodnotit class a v deruhem provedeni ne. jako uvod makra
    to neni rule.
    ------------------
    Nezapomen jako lastClass dat v initu Rule. No, jeste to prober. Nemuze napr. zacinat
    operatorem nebo pravou zavorkou.


*******************************************************************************/


/*----------------------------------------------------------------------------*/
/* Includes                                                                   */
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include "config.h"
#include "prepare.h"


/*----------------------------------------------------------------------------*/
/* Definice konstatnt                                                         */
/*----------------------------------------------------------------------------*/

const prepare_clerror_t
  Prepare_ClErrorTab[PREPARE_CNT_RULES] = {
  { PREPARE_SYMBOL_ANYSPACE       , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_ONEORMORESPACE , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_STATICTEXT     , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_VARIABLETEXT   , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_SYNCTEXT       , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_VARIABLENUMBER , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_SKIPTEXT       , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_OR             , PREPARE_CLASS_OPERATORS},
  { PREPARE_SYMBOL_AND            , PREPARE_CLASS_OPERATORS},
  { PREPARE_SYMBOL_ENDBLOCK       , PREPARE_CLASS_OPERATORS},
  { PREPARE_SYMBOL_INITVALS       , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_CALLTASK       , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_MACRO          , PREPARE_CLASS_RULE     },
  { PREPARE_SYMBOL_LBRACKET       , PREPARE_CLASS_LBRACKET },
  { PREPARE_SYMBOL_LOPTION        , PREPARE_CLASS_LBRACKET },
  { PREPARE_SYMBOL_LREPEAT        , PREPARE_CLASS_LBRACKET },
  { PREPARE_SYMBOL_PBRACKET       , PREPARE_CLASS_PBRACKET },
  { PREPARE_SYMBOL_POPTION        , PREPARE_CLASS_PBRACKET },
  { PREPARE_SYMBOL_PREPEAT        , PREPARE_CLASS_PBRACKET }
};

/* Ve funkci prepare_clerror_t prepare_get_clerror(char symbol) se vybira radek tabulky
 * podle symbolu. A kdyz je symbol chybny a nevybere se nic, tak se dosadi tato hodnota.
 */
const prepare_clerror_t
  Prepare_ClError = { PREPARE_ERROR_VALUE, PREPARE_CLASS_RULE };

const char *
  Prepare_ErrTextTab[] = {
  "Incorrect move range of rule                    ",  // n RULE_ERR_ASPACE       Chybne chrscale
  "Incorrect move range of rule                    ",  // n RULE_ERR_OMSPACE      Chybne chrscale
  "Incorrect directive format STATICTEXT rule      ",  // n RULE_ERR_STATICTEXT1  reverzer je, ale druhej neni ani " ani <  -  ale reverser a whitechl nejsou stejne
  "Incorrect format of STATICTEXT rule             ",  // a RULE_ERR_STATICTEXT2  neni reverzer a totez, spatny format
  "Missing right quote of STATICTEXT rule          ",  // a RULE_ERR_STATICTEXT3  koncova uvozovka
  "Missing right bracket '>' of whitechar          ",  // a RULE_ERR_STATICTEXT4  prava zavorka white
  "Incorrect character of whitechar                ",  // a RULE_ERR_STATICTEXT5  spatny whitechar
  "Incorrect move range of rule                    ",  // n RULE_ERR_VARTEXT1     Chybne chrscale
  "Incorrect type of right bracket of text         ",  // a RULE_ERR_VARTEXT2     prava zavorka normalniho textu
  "Incorrect right bracket '>' of whitechar        ",  // a RULE_ERR_VARTEXT3     prava zavorka bileho znaku
  "Incorrect code of white char                    ",  // a RULE_ERR_VARTEXT4     zkratka bileho znaku
  "Incorrect type of VARIABLETEXT                  ",  // a RULE_ERR_VARTEXT5     druh textu
  "Incorrect move range of SYNCTEXT rule           ",  // n RULE_ERR_SYNCTEXT1    Chybne chrscale
  "Symbols REVERSER and WHITECHL are not the same  ",  // a RULE_ERR_SYNCTEXT2
  "Missing left bracket of white char or text quote",  // a RULE_ERR_SYNCTEXT3    chybi uvodni uvozovka nebo zavorka
  "Missing right quote of text                     ",  // n RULE_ERR_SYNCTEXT4
  "Missing right bracket of white char             ",  // a RULE_ERR_SYNCTEXT5
  "Incorrect code of white char                    ",  // a RULE_ERR_SYNCTEXT6
  "Incorrect move range of rule                    ",  // n RULE_ERR_VARNUMBER1   Chybne chrscale
  "Incorrect size specifier of VARIABLENUM         ",  // a RULE_ERR_VARNUMBER2   chybne nsize
  "Incorrect format specifier of VARIABLENUM       ",  // a RULE_ERR_VARNUMBER3   chybny format
  "Incorrect move range of rule                    ",  // n RULE_ERR_SKIPTEXT     Chybne chrscale
  "Incorrect Id of callback                        ",  // a RULE_ERR_CALL         chybne cislo callbacku
  "Incorrect format of MACRO rule                  ",  // a RULE_ERR_MACRO1       Chybny format cisla Id
  "Incorrect Id of MACRO rule                      ",  // a RULE_ERR_MACRO2       Chybna hodnota Id
  "Unknown Id of MACRO rule                        ",  // a RULE_ERR_MACRO3       Nebylo nalezeno makro
  "Exceeding maximum nesting level                 ",  // a RULE_ERR_LBRACKET     Prekroceni max. urovne vnoreni Level
  "Incorrect type of right bracket                 ",  // n RULE_ERR_PBRACKET1    jiny kod nez prava zavorka
  "Left and right brackets do not match            ",  // a RULE_ERR_PBRACKET2    zavorky nejsou do paru, ulozena leva neodpovida prave
  "Probably odd number of brackets                 ",  // RULE_ERR_PBRACKET3    level je nula, neni mozne ho snizit. asi lichy pocet zavorek
  "Exceeding the maximum number of rules           ",  // a RULE_ERR_SOLVE1       Prekrocen maximalni pocet rules
  "Incorrect symbol of rule or missing right quote.",  // a RULE_ERR_SOLVE2       Chybny symbol rule. Kdyz chybi prava uvozovka a zastavi na jine, je to taky zde.

  "No error                                        ",  // n CLASS_ERR_NOERROR     neni chyba
  "Script starts with the operator                 ",  // a CLASS_ERR_EXPROPER    vyraz zacina operatorem
  "Script starts with the right bracket            ",  // a CLASS_ERR_EXPRPBRA    vyraz zacina pravou zavorkou
  "Duplicated operators                            ",  // a CLASS_ERR_EXPRDUBL    dva operatory za sebou
  "Missing right part of expression                ",  // a CLASS_ERR_MISSRIGHT   chybi prava cast vyrazu
  "Missing left part of expression                 ",  // a CLASS_ERR_MISSLEFT    chybi leva cast vyrazu
  "Internal error, unknown class combination       ",  // n CLASS_ERR_MISSCLASS   kombinace class nenalezena v tabulce

  "Unexpected end of line or missing ';'           ",  // a AREA_ERR_RULECRLF     chybi ; konec radku uprostred vyrazu nebo makra
  "Comment symbol inside expression                ",  // a AREA_ERR_RULECOMM     znak komentu uprostred vyrazu. nepripustne unacceptable
  "Unexpected end of file or missing ';'           ",  // a AREA_ERR_RULEEND      konec souboru uprostred vyrazu. Chybi ;
  "Odd number of brackets                          ",  // a AREA_ERR_RULELEVEL    Level neni 0, coz je Lichy pocet zavorek.
  "Empty script or macro                           ",  // a AREA_ERR_AFTERMACRO   Zadne rules v makru  nebo hlavnim scriptu
  "Coupler symbol beyond expression                ",  // a AREA_ERR_AFTERCOUPL   za koncem  macra nebo hl. scriptu nemusi byt
  "Unexpected end of line or Missing ';'           ",  // a AREA_ERR_AFTEREND     chybi strednik, konec souboru  je driv
  "Unacceptable chars after coupler symbol         ",  // a AREA_ERR_COUPLER1     za couplerem nesmi byt znaky krome komentu
  "Missing part of expression after coupler        ",  // a AREA_ERR_COUPLER2     na dalsim radku za couplerem nesmi byt ; coupler a konec souboru
  "Unexpected end of file after comment            ",  // a AREA_ERR_COMMENT      je konec souboru a za komentem je nedokonceny script
  "Unknown area code                               ",  // n AREA_ERR_GLOBAL       neznamy kod area
  "Unacceptable rules beyond end of expression     ",  // a AREA_ERR_INIPRIMARY   V hlavnim vyrazu hlasi makra. Neprevdepodobna chyba
  "Incorrect syntax of MACRO. Wrong id             ",  // n AREA_ERR_INIMACRO1    Chybne zapsane id makra
  "Incorrect syntax of MACRO. Missing ':'          ",  // a AREA_ERR_INIMACRO2    Chybi : za Id makra
  "Incorrect id value of MACRO                     ",  // a AREA_ERR_INIMACRO3    Chybna hodnota id makra
  "Exceeding maximum number of MACRO               ",  // n AREA_ERR_INIMACRO4    Prilis velky pocet maker
  "Duplicate value of MACRO Id                     ",  // a AREA_ERR_INIMACRO5    Duplicitni Id makra
  "Exceeding maximum number of MACRO               ",  // n AREA_ERR_INIMACRO6    Prilis velky pocet maker pri pokusu ho vytvorit
  "Odd number of brackets                          ",  // n AREA_ERR_ENDSCRIPT    Lichy pocet zavorek

  "*********                                       ",  // CODE_ERR_COPYRULE
  "Missing ';' at the end of the expression        ",  // CODE_ERR_COPYSELF     Chybi ; na konci vyrazu
  "Mismatch in brackets                            ",  // CODE_ERR_COPYCODE1    parove zavorky nejsou stejneho typu
  "Unexpected symbol ';' inside expression         ",  // CODE_ERR_COPYCODE2
  "Unexpected end of file or odd number of brackets",  // CODE_ERR_COPYCODE3    nedokoncene zavorky, konec scriptu mezi zavorkami
  "Timeout error                                   "   // CODE_ERR_TIMEOUT      timeout behem preskupeni
};

/*----------------------------------------------------------------------------*/
/* Deklarace externich funkci                                                 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* Definice vlastnich funkci                                                  */
/*----------------------------------------------------------------------------*/


/*******************************************************************************
 * Funkce slouzi pouze k predani ukazatele scriptu do objektu. Parsing se touto
 *  funkci nespousti.
 *  pself->ScriptTextPtr = ukazatel na zacatek scriptu. Ten se nemeni.
 *  pself->ScriptCharPtr = ukazatel na aktualni znak. Ten se meni.
 */
void prepare_init_script(prepare_main_t * pself, char * pscript)
{
  pself->ScriptTextPtr = pscript;
  pself->ScriptCharPtr = pscript;

  pself->ScriptArea = PREPARE_AREA_IDLE;
}

/*******************************************************************************
 * Funkce vraci pozici ukazatele ScriptCharPtr jako index.
 */
unsigned short prepare_count_rulepos(prepare_main_t * pself)
{
  return ((unsigned short)(pself->ScriptCharPtr - pself->ScriptTextPtr));
}

/*******************************************************************************
 * Funkce
 */
#ifdef __ALLOW_DEBUG_DUMP
  void prepare_main_dump(prepare_main_t * pself, char * dprefix)
  {
    char dsymbol;

    switch (*pself->ScriptCharPtr) {
      case PREPARE_SYMBOL_ENDROWLF : dsymbol = 'N'; break;
      case PREPARE_SYMBOL_ENDROWCR : dsymbol = 'R'; break;
      case PREPARE_SYMBOL_ENDSCRIPT: dsymbol = '0'; break;
      default: dsymbol = *pself->ScriptCharPtr; break;
    }
    if (*dprefix == ' ') {
       printf("\r\n%s %c %03d %03d  %02d  %02d  %01d ", dprefix, dsymbol, prepare_count_rulepos(pself), pself->RuleAct, pself->RowPos, pself->ColPos, pself->LevelAct);
    }
    else {
       printf("\r\n%s %c %03d ---  %02d  %02d  %01d ", dprefix, dsymbol, prepare_count_rulepos(pself), pself->RowPos, pself->ColPos, pself->LevelAct);
    }
  }
#endif

/*******************************************************************************
 *Funkce
 */
void prepare_start_analysis(prepare_main_t * pself)
{
  pself->LevelAct   = 0;
  pself->MacroCnt   = 0;
  pself->LastClass  = PREPARE_CLASS_BEGIN;
  pself->RuleAct    = 0;
  pself->RowPos     = 0;
  pself->DataPtr    = pself->DataTab;
  pself->ColPos     = 0;

  pself->ScriptCharPtr = pself->ScriptTextPtr;   
  
  pself->RawTextPtr = pself->RawTextStr;

  pself->ScriptMode = PREPARE_MODE_PRIMARYRULES;
  pself->ScriptArea = PREPARE_AREA_AFTEREND;

  #ifdef __ALLOW_DEBUG_DUMP
    printf("\r\n---------------------------------------------------------------");
    printf("\r\nZACATEK ZAKLADNIHO PARSINGU");
    printf("\r\n---------------------------------------------------------------");
    printf("\r\nST S POS ACT ROW COL LEV  ");
    printf("\r\n---------------------------------------------------------------");
    prepare_main_dump(pself, "AF");
    printf(" START");
  #endif
}

/*******************************************************************************
 *Funkce
 */
void prepare_display_errortype(prepare_errors_e errtype)
{
  switch (errtype) {
    case PREPARE_RULE_ERR_ASPACE     : printf("[ASPACE]");      break;
    case PREPARE_RULE_ERR_OMSPACE    : printf("[OMSPACE]");     break;
    case PREPARE_RULE_ERR_STATICTEXT1: printf("[STATICTEXT1]"); break;
    case PREPARE_RULE_ERR_STATICTEXT2: printf("[STATICTEXT2]"); break;
    case PREPARE_RULE_ERR_STATICTEXT3: printf("[STATICTEXT3]"); break;
    case PREPARE_RULE_ERR_STATICTEXT4: printf("[STATICTEXT4]"); break;
    case PREPARE_RULE_ERR_STATICTEXT5: printf("[STATICTEXT5]"); break;
    case PREPARE_RULE_ERR_VARTEXT1   : printf("[VARTEXT1]");    break;
    case PREPARE_RULE_ERR_VARTEXT2   : printf("[VARTEXT2]");    break;
    case PREPARE_RULE_ERR_VARTEXT3   : printf("[VARTEXT3]");    break;
    case PREPARE_RULE_ERR_VARTEXT4   : printf("[VARTEXT4]");    break;
    case PREPARE_RULE_ERR_VARTEXT5   : printf("[VARTEXT5]");    break;
    case PREPARE_RULE_ERR_SYNCTEXT1  : printf("[SYNCTEXT1]");   break;
    case PREPARE_RULE_ERR_SYNCTEXT2  : printf("[SYNCTEXT2]");   break;
    case PREPARE_RULE_ERR_SYNCTEXT3  : printf("[SYNCTEXT3]");   break;
    case PREPARE_RULE_ERR_SYNCTEXT4  : printf("[SYNCTEXT4]");   break;
    case PREPARE_RULE_ERR_SYNCTEXT5  : printf("[SYNCTEXT5]");   break;
    case PREPARE_RULE_ERR_SYNCTEXT6  : printf("[SYNCTEXT6]");   break;
    case PREPARE_RULE_ERR_VARNUMBER1 : printf("[VARNUMBER1]");  break;
    case PREPARE_RULE_ERR_VARNUMBER2 : printf("[VARNUMBER2]");  break;
    case PREPARE_RULE_ERR_VARNUMBER3 : printf("[VARNUMBER3]");  break;
    case PREPARE_RULE_ERR_SKIPTEXT   : printf("[SKIPTEXT]");    break;
    case PREPARE_RULE_ERR_CALL       : printf("[CALL]");        break;
    case PREPARE_RULE_ERR_MACRO1     : printf("[MACRO1]");      break;
    case PREPARE_RULE_ERR_MACRO2     : printf("[MACRO2]");      break;
    case PREPARE_RULE_ERR_MACRO3     : printf("[MACRO3]");      break;
    case PREPARE_RULE_ERR_LBRACKET   : printf("[LBRACKET]");    break;
    case PREPARE_RULE_ERR_PBRACKET1  : printf("[PBRACKET1]");   break;
    case PREPARE_RULE_ERR_PBRACKET2  : printf("[PBRACKET2]");   break;
    case PREPARE_RULE_ERR_PBRACKET3  : printf("[PBRACKET3]");   break;
    case PREPARE_RULE_ERR_SOLVE1     : printf("[SOLVE1]");      break;
    case PREPARE_RULE_ERR_SOLVE2     : printf("[SOLVE2]");      break;
    //----------------------------------------------------------------
    case PREPARE_CLASS_ERR_NOERROR   : printf("[NOERROR]");     break;
    case PREPARE_CLASS_ERR_EXPROPER  : printf("[EXPROPER]");    break;
    case PREPARE_CLASS_ERR_EXPRPBRA  : printf("[EXPRPBRA]");    break;
    case PREPARE_CLASS_ERR_EXPRDUBL  : printf("[EXPRDUBL]");    break;
    case PREPARE_CLASS_ERR_MISSRIGHT : printf("[MISSRIGHT]");   break;
    case PREPARE_CLASS_ERR_MISSLEFT  : printf("[MISSLEFT]");    break;
    case PREPARE_CLASS_ERR_MISSCLASS : printf("[MISSCLASS]");   break;
    //----------------------------------------------------------------
    case PREPARE_AREA_ERR_RULECRLF   : printf("[RULECRLF]");    break;
    case PREPARE_AREA_ERR_RULECOMM   : printf("[RULECOMM]");    break;
    case PREPARE_AREA_ERR_RULEEND    : printf("[RULEEND]");     break;
    case PREPARE_AREA_ERR_RULELEVEL  : printf("[RULELEVEL]");   break;
    case PREPARE_AREA_ERR_AFTERMACRO : printf("[AFTERMACRO]");  break;
    case PREPARE_AREA_ERR_AFTERCOUPL : printf("[AFTERCOUPL]");  break;
    case PREPARE_AREA_ERR_AFTEREND   : printf("[AFTEREND]");    break;
    case PREPARE_AREA_ERR_COUPLER1   : printf("[COUPLER1]");    break;
    case PREPARE_AREA_ERR_COUPLER2   : printf("[COUPLER2]");    break;
    case PREPARE_AREA_ERR_COMMENT    : printf("[COMMENT]");     break;
    case PREPARE_AREA_ERR_GLOBAL     : printf("[GLOBAL]");      break;
    case PREPARE_AREA_ERR_INIPRIMARY : printf("[INIPRIMARY]");  break;
    case PREPARE_AREA_ERR_INIMACRO1  : printf("[INIMACRO1]");   break;
    case PREPARE_AREA_ERR_INIMACRO2  : printf("[INIMACRO2]");   break;
    case PREPARE_AREA_ERR_INIMACRO3  : printf("[INIMACRO3]");   break;
    case PREPARE_AREA_ERR_INIMACRO4  : printf("[INIMACRO4]");   break;
    case PREPARE_AREA_ERR_INIMACRO5  : printf("[INIMACRO5]");   break;
    case PREPARE_AREA_ERR_INIMACRO6  : printf("[INIMACRO6]");   break;
    case PREPARE_AREA_ERR_ENDSCRIPT  : printf("[ENDSCRIPT]");   break;
    //----------------------------------------------------------------
    case PREPARE_CODE_ERR_COPYRULE   : printf("[COPYRULE]");    break;
    case PREPARE_CODE_ERR_COPYSELF   : printf("[COPYSELF]");    break;
    case PREPARE_CODE_ERR_COPYCODE1  : printf("[COPYCODE1]");   break;
    case PREPARE_CODE_ERR_COPYCODE2  : printf("[COPYCODE2]");   break;
    case PREPARE_CODE_ERR_COPYCODE3  : printf("[COPYCODE3]");   break;
    case PREPARE_CODE_ERR_TIMEOUT    : printf("[TIMEOUT]");     break;
  }
}

/*******************************************************************************
 *Funkce zatim zapise chybu a dalsi doprovodne udaje do jedne promenne.
 *  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *  Nutno vice rozvinout. Zridit jednak pole a ne jednoduchou promennou
 *  a vytvorit dve tridy - RuleErr a DataErr aby bylo videt, kde je chyba.
 *
 */
void prepare_raise_error(prepare_main_t * pself, prepare_errors_e errtype)
{
  pself->ErrorAct.Type = errtype;
  pself->ErrorAct.RuleAct = pself->RuleAct;
  pself->ErrorAct.RowPos = pself->RowPos;
  pself->ErrorAct.ColPos = pself->ColPos;

  pself->ScriptArea = PREPARE_AREA_ERROR;

  #ifdef __ALLOW_DEBUG_DUMP
    printf("\r\n---------------------------------------------------------------");
    printf("\r\nERR:");
    prepare_display_errortype(errtype);
    printf(",Rule:%d,Row:%d,Col:%d ", pself->ErrorAct.RuleAct, pself->ErrorAct.RowPos, pself->ErrorAct.ColPos);
  #endif
}

/*******************************************************************************
 * Funkce
 *
 *
 *
 *
 */
prepare_clerror_t prepare_get_clerror(char symbol)
{
  unsigned char i;

  for (i = 0; i < PREPARE_CNT_RULES; i++) {
    if (Prepare_ClErrorTab[i].ClassSymbol == symbol) {
      return (Prepare_ClErrorTab[i]);
    }
  }
  return (Prepare_ClError);
}

/*******************************************************************************
 * Funkce najde v tabulce kombinaci, ktera je zadana parametry a vrati povolovaci
 * vysledek.
 * Zakazany jsou tyto kombinace: .. .| .) |. || |) (. (| (; |; .;
 *  neni chyba                               PREPARE_CLASS_ERR_NOERROR
 *  vyraz zacina operatorem                  PREPARE_CLASS_ERR_EXPROPER
 *  vyraz zacina pravou zavorkou             PREPARE_CLASS_ERR_EXPRPBRA
 *  dva operatory za sebou                   PREPARE_CLASS_ERR_EXPRDUBL
 *  chybi prava cast vyrazu                  PREPARE_CLASS_ERR_MISSRIGHT
 *  chybi leva cast vyrazu                   PREPARE_CLASS_ERR_MISSLEFT
 *  kombinace class nenalezena v tabulce     PREPARE_CLASS_ERR_MISSCLASS
 */
char prepare_check_class(prepare_class_e * p_last_class, prepare_class_e cur_class)
{
  unsigned char i;

  const prepare_clpairs_t
  class_pair_tab[PREPARE_CNT_CLASSES * PREPARE_CNT_CLASSES - PREPARE_CNT_CLASSES] = {
    {PREPARE_CLASS_BEGIN,     PREPARE_CLASS_RULE      ,PREPARE_CLASS_ERR_NOERROR  }, // 0
    {PREPARE_CLASS_BEGIN,     PREPARE_CLASS_OPERATORS ,PREPARE_CLASS_ERR_EXPROPER }, // 1
    {PREPARE_CLASS_BEGIN,     PREPARE_CLASS_LBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }, // 2
    {PREPARE_CLASS_BEGIN,     PREPARE_CLASS_PBRACKET  ,PREPARE_CLASS_ERR_EXPRPBRA }, // 3

    {PREPARE_CLASS_RULE,      PREPARE_CLASS_RULE      ,PREPARE_CLASS_ERR_NOERROR  }, // 4
    {PREPARE_CLASS_RULE,      PREPARE_CLASS_OPERATORS ,PREPARE_CLASS_ERR_NOERROR  }, // 5
    {PREPARE_CLASS_RULE,      PREPARE_CLASS_LBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }, // 6
    {PREPARE_CLASS_RULE,      PREPARE_CLASS_PBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }, // 7

    {PREPARE_CLASS_OPERATORS, PREPARE_CLASS_RULE      ,PREPARE_CLASS_ERR_NOERROR  }, // 8
    {PREPARE_CLASS_OPERATORS, PREPARE_CLASS_OPERATORS ,PREPARE_CLASS_ERR_EXPRDUBL }, // 9
    {PREPARE_CLASS_OPERATORS, PREPARE_CLASS_LBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }, // 10
    {PREPARE_CLASS_OPERATORS, PREPARE_CLASS_PBRACKET  ,PREPARE_CLASS_ERR_MISSRIGHT}, // 11

    {PREPARE_CLASS_LBRACKET,  PREPARE_CLASS_RULE      ,PREPARE_CLASS_ERR_NOERROR  }, // 12
    {PREPARE_CLASS_LBRACKET,  PREPARE_CLASS_OPERATORS ,PREPARE_CLASS_ERR_MISSLEFT }, // 13
    {PREPARE_CLASS_LBRACKET,  PREPARE_CLASS_LBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }, // 14
    {PREPARE_CLASS_LBRACKET,  PREPARE_CLASS_PBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }, // 15

    {PREPARE_CLASS_PBRACKET,  PREPARE_CLASS_RULE      ,PREPARE_CLASS_ERR_NOERROR  }, // 16
    {PREPARE_CLASS_PBRACKET,  PREPARE_CLASS_OPERATORS ,PREPARE_CLASS_ERR_NOERROR  }, // 17
    {PREPARE_CLASS_PBRACKET,  PREPARE_CLASS_LBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }, // 18
    {PREPARE_CLASS_PBRACKET,  PREPARE_CLASS_PBRACKET  ,PREPARE_CLASS_ERR_NOERROR  }  // 19
  };

  for (i = 0; i < (PREPARE_CNT_CLASSES * PREPARE_CNT_CLASSES) - PREPARE_CNT_CLASSES; i++) {
    if ((*p_last_class == class_pair_tab[i].Last) && (cur_class == class_pair_tab[i].Cur)) {
      *p_last_class = cur_class;
      #ifdef __ALLOW_DEBUG_DUMP
        printf(" Cl.tst:%d, %d ", i, class_pair_tab[i].Result);
      #endif
      return (class_pair_tab[i].Result);
    }
  }
  return (PREPARE_CLASS_ERR_MISSCLASS);
}


/*******************************************************************************
 * Funkce najde kod bileho znaku ze zkratky na kterou ukazuje pself->ScriptCharPtr.
 * Pozor. Navratova hodnota ma bohuzel dve funkce. Bud ASCII znak v rozsahu 0..31
 * pokud je rozpoznan spravne nebo -1 jako chybove hlaseni, pokud nebyl rozpoznan.
 * pself->ScriptCharPtr ukazuje tesne za text zkratky.
 */
int prepare_get_whitechar(prepare_main_t * pself)
{
  char * set_ptr;
  int ascii_tab[] = {6 ,7 ,8 ,24,13,17,18,19,20,16,25,5 ,4 ,27,23,3 ,12,28,29,10,21,0 ,30,15,1 ,14,2 ,22,26,9 ,31,11};
  char set_tab[] = {"ACKBELBS.CANCR.DC1DC2DC3DC4DLEEM.ENQEOTESCETBETXFF.FS.GS.LF.NAKNULRS.SI.SOHSO.STXSYNSUBTABUS.VT.\0\0\0"};
  //                 `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `  `
  //                 0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1

  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! seradit podle kodu od 0 a pak se muze pole s usporadanim vynechat

  set_ptr = set_tab;

  while (*set_ptr != '\0') {
    if (*pself->ScriptCharPtr == *set_ptr) {
      set_ptr++;
      pself->ScriptCharPtr++;
      pself->ColPos++;
      while (*set_ptr != '\0') {
        if (*pself->ScriptCharPtr == *set_ptr) {
          set_ptr++;
          pself->ScriptCharPtr++;
          pself->ColPos++;
          while (*set_ptr != '\0') {
            if ((*pself->ScriptCharPtr != *set_ptr) && (*set_ptr != '.')) {
              set_ptr += 3;
            }
            else {
              if (*set_ptr != '.') {
                pself->ScriptCharPtr++;
                pself->ColPos++;
              }
              return (ascii_tab[(int)(set_ptr - set_tab) / 3]);
            }
          }
        }
        else {
          set_ptr += 3;
        }
      }
    }
    else {
      set_ptr += 3;
    }
  }
  return (-1);
}

/*******************************************************************************
 *  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *  Tato funkce se mi nezda. Jak zajisti, ze nepretece radek az nekam
 *
 *
 */
void prepare_skip_spaces(prepare_main_t * pself)
{
  unsigned short i;

  for (i = 0; i < PREPARE_MAXLEN_WHOLESCRIPT; i++) {
    if (*pself->ScriptCharPtr != PREPARE_SYMBOL_NATURALSPACE) {
      return;
    }
    else {
      pself->ScriptCharPtr++;
      pself->ColPos++;
    }
  }
}

/*******************************************************************************
 * Blokujici funkce, ktera prevede cislo pozadovaneho typu do binarni formy.
 * Prevod konci prvnim znakem, ktery nesplnuje podminky daneho typu. Jeden
 * cyklus funkce ascanf_processing() se s timto znakem ale musi vykonat.
 * Proto je pocet volani teto funkce o 1 vetsi nez je delka prevedeneho cisla.
 *
 */
void prepare_scanf_number(prepare_main_t * pself)
{
  unsigned char i,stlen;
  ascanf_status_e rstatus;
  char * pString;

  pString = pself->ScriptCharPtr;

  ascanf_init(&(pself->ScanfSession), ASCANF_NFORMAT_UNSIGNED);
  ascanf_start(&(pself->ScanfSession));
  stlen = 0;
  for (i = 0; i < ASCANF_MAX_NUMSTRING; i++) {
    rstatus = ascanf_processing(&(pself->ScanfSession), *pString);
    if (rstatus == ASCANF_STATUS_CONTINUE) {
      pString++;
      stlen++;
    }
    else {
      pself->ScanfSession.RStatus = stlen;
      return;
    }
  }
}

/******************************************************************************/
/******************************************************************************/


/*******************************************************************************
 * Funkce vrati levou zavorku misto prislusne prave.
 *111!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * Doplnit *; a ;*
 */
char prepare_invert_bracket(char lpbracket)
{
  switch (lpbracket) {
      case PREPARE_SYMBOL_PBRACKET: return(PREPARE_SYMBOL_LBRACKET); break;
      case PREPARE_SYMBOL_POPTION : return(PREPARE_SYMBOL_LOPTION ); break;
      case PREPARE_SYMBOL_PREPEAT : return(PREPARE_SYMBOL_LREPEAT ); break;

      case PREPARE_SYMBOL_LBRACKET: return(PREPARE_SYMBOL_PBRACKET); break;
      case PREPARE_SYMBOL_LOPTION : return(PREPARE_SYMBOL_POPTION ); break;
      case PREPARE_SYMBOL_LREPEAT : return(PREPARE_SYMBOL_PREPEAT ); break;

      default: return ('\0'); break;
  }
}

/*******************************************************************************
 * Funkce vrati 
 *
 * 
 */
prepare_status_e prepare_skiptext_type(prepare_skiptype_e type)
{
  switch (type) {
    case PREPARE_SKIPTYPE_NUMERIC  :
    case PREPARE_SKIPTYPE_CHARACTER:
    case PREPARE_SKIPTYPE_SYMBOL   :
    case PREPARE_SKIPTYPE_WHITE    :
      return PREPARE_STATUS_OK;
    break;
    default: 
      return PREPARE_STATUS_ERR;
    break;  
  }
}

/*******************************************************************************
 *
 *
 */
char prepare_solve_reverse(prepare_main_t * pself)
{
  if (*pself->ScriptCharPtr == PREPARE_SYMBOL_REVERSER) {
    #ifdef __ALLOW_DEBUG_DUMP
      printf("Reverse ");
    #endif

    pself->ScriptCharPtr++;
    pself->ColPos++;  
    
    *pself->RawTextPtr++ = PREPARE_SYMBOL_REVERSER;
    
    return (PREPARE_SYMBOL_REVERSER);
  }
  else {
    return (PREPARE_SYMBOL_NATURALSPACE);
  }
}

/*******************************************************************************
 *
 *
 */
void prepare_solve_whitechar_raw(prepare_main_t * pself, char * ptr_ch)
{
  ptr_ch = pself->ScriptCharPtr;
  *pself->RawTextPtr++ = *ptr_ch++;
  *pself->RawTextPtr++ = *ptr_ch++;
  if (*ptr_ch == PREPARE_DELIMIT_WHITECHR) {
    *pself->RawTextPtr++ = *ptr_ch++;
  }
  else {  
    *pself->RawTextPtr++ = *ptr_ch++;
    *pself->RawTextPtr++ = *ptr_ch++;
  }
} 

/*******************************************************************************
 * Tato funkce je naprosty antipatern. Je typu int a pritom jako hodnotu
 * dava bud typ status a nebo pocetznaku. To uz by bylo lepsi nechat to cislo mensi
 * nez 0 a to pak vyhodnocovat. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
int prepare_solve_chrscale(prepare_main_t * pself, prepare_errors_e error)
{
  if ((*pself->ScriptCharPtr >= '0') && (*pself->ScriptCharPtr <= '9')) {
    prepare_scanf_number(pself);
    if (pself->ScanfSession.RStatus < 0) {
      prepare_raise_error(pself, error);
      return (PREPARE_STATUS_ERR);
    }
    else {
      #ifdef __ALLOW_DEBUG_DUMP
        printf("ChrScale:%d ", pself->ScanfSession.Work.Val32);
      #endif

      pself->ScriptCharPtr += pself->ScanfSession.RStatus;
      pself->ColPos += pself->ScanfSession.RStatus;
                        
      itoa(pself->ScanfSession.Work.Val32, pself->RawTextPtr, 10); 
      pself->RawTextPtr += pself->ScanfSession.RStatus;
  
      return (pself->ScanfSession.Work.Val32);
    }
  }
  else {
    return (0);
  }
}

/*******************************************************************************
 * Funkce analyzuje syntax rule ANYSPACE. Varianty: -  -24 -<24
 */
void prepare_solve_anyspace(prepare_main_t * pself)
{
  prepare_data_anyspace_t * anyspace_ptr;
  unsigned short chrscale;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" ANYSPACE: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  anyspace_ptr = (prepare_data_anyspace_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_anyspace_t);

  anyspace_ptr->Reverser = prepare_solve_reverse(pself);

  chrscale = prepare_solve_chrscale(pself, PREPARE_RULE_ERR_ASPACE);
  if (chrscale != PREPARE_STATUS_ERR) {
    anyspace_ptr->ChrScale = chrscale;
  }
}

/*******************************************************************************
 * Funkce analyzuje syntax rule ONEORMORESPACE  Varianty: =  =24 =<24
 */
void prepare_solve_oneormorespace(prepare_main_t * pself)
{
  prepare_data_oneormorespace_t * oneormorespace_ptr;
  unsigned short chrscale;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" ONEORMORESPACE: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  oneormorespace_ptr = (prepare_data_oneormorespace_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_oneormorespace_t);

  oneormorespace_ptr->Reverser = prepare_solve_reverse(pself);

  chrscale = prepare_solve_chrscale(pself, PREPARE_RULE_ERR_OMSPACE);
  if (chrscale != PREPARE_STATUS_ERR) {
    oneormorespace_ptr->ChrScale = chrscale;
  }
}

/*******************************************************************************
 * Funkce analyzuje syntax rule STATICTEXT Varianty: %"Text" %<CR> %<"Text" %<<CR>
 * Funkce zkontroluje bud retezec, jestli je v uvozovkach nebo zkontroluje
 * spravnost zkratky bileho znaku.
 * Pozor, za symbolem nesmi byt zadna mezera, musi byt rovnou uvozovka.
 * Index RuleAct musi ukazovat na aktualni rule. Pozor na to, kdy ho inkrementujes.
 * Clen StructPtr musi byt jiz naplnen ukazatelem DataPtr, coz se dela v nadrazene funkci.
 * Text je ulozen i s uvozovkama a zakoncen \0.
 * Bily znak je ulozen ve tvaru leva ostra zavorka ascii znaku a znak \0.
 */
void prepare_solve_statictext(prepare_main_t * pself)
{
  unsigned short i;
  int white_char;     
  prepare_data_statictext_t * statictext_ptr;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" STATICTEXT: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  statictext_ptr = (prepare_data_statictext_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_statictext_t);

  statictext_ptr->TextIdx = (unsigned short)(pself->DataPtr - pself->DataTab);
      
  *pself->RawTextPtr++ = *pself->ScriptCharPtr;
      
  if (*pself->ScriptCharPtr == PREPARE_SYMBOL_REVERSER) {
    if (*pself->ScriptCharPtr == PREPARE_DELIMIT_WHITECHL) {
      pself->ScriptCharPtr++;
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! a co inkrementovat  pself->ColPos++;

      if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
        *pself->RawTextPtr++ = *pself->ScriptCharPtr;
        statictext_ptr->Reverser = PREPARE_SYMBOL_REVERSER;
        statictext_ptr->Type = PREPARE_TXSTATIC_NORMAL;
      }
      else {
        if (*pself->ScriptCharPtr == PREPARE_DELIMIT_WHITECHL) {
          *pself->RawTextPtr++ = *pself->ScriptCharPtr;          
          pself->ScriptCharPtr++;                                                 
          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! a co inkrementovat  pself->ColPos++;
          statictext_ptr->Reverser = PREPARE_SYMBOL_REVERSER;
          statictext_ptr->Type = PREPARE_TXSTATIC_WHITE;
        }
        else {
          statictext_ptr->Reverser = PREPARE_SYMBOL_NATURALSPACE;
          statictext_ptr->Type = PREPARE_TXSTATIC_WHITE;
        }
      }
    }
    else {
      statictext_ptr->Type = PREPARE_TXSTATIC_ERROR;
      prepare_raise_error(pself, PREPARE_RULE_ERR_STATICTEXT1);
    }
  }
  else {
    statictext_ptr->Reverser = PREPARE_SYMBOL_NATURALSPACE;
    if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
      statictext_ptr->Type = PREPARE_TXSTATIC_NORMAL;
    }
    else {
      if (*pself->ScriptCharPtr == PREPARE_DELIMIT_WHITECHL) {
        statictext_ptr->Type = PREPARE_TXSTATIC_WHITE;
      }
      else {
        statictext_ptr->Type = PREPARE_TXSTATIC_ERROR;
        prepare_raise_error(pself, PREPARE_RULE_ERR_STATICTEXT2);
      }
    }
  }

  #ifdef __ALLOW_DEBUG_DUMP
    if (statictext_ptr->Reverser == PREPARE_SYMBOL_REVERSER) {
      printf(" REVERSE ");
    }
  #endif

  if (statictext_ptr->Type == PREPARE_TXSTATIC_NORMAL) {
    *pself->DataPtr++ = *pself->ScriptCharPtr;
    for (i = 0; i < PREPARE_MAXLEN_STATICTEXT; i++) {
      #ifdef __ALLOW_DEBUG_DUMP
        printf("%c", *pself->ScriptCharPtr);
      #endif
      pself->ScriptCharPtr++;
      pself->ColPos++;
      *pself->DataPtr++ = *pself->ScriptCharPtr;
      *pself->RawTextPtr++ = *pself->ScriptCharPtr;
      if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
        *pself->DataPtr++ = '\0';
        pself->ScriptCharPtr++;
        pself->ColPos++;
        return;
      }
      if (*pself->ScriptCharPtr == PREPARE_SYMBOL_ENDSCRIPT) {
        prepare_raise_error(pself, PREPARE_RULE_ERR_STATICTEXT3);
      }
    }
    prepare_raise_error(pself, PREPARE_RULE_ERR_STATICTEXT3);
  }
  else {
    if (statictext_ptr->Type == PREPARE_TXSTATIC_WHITE) {
      *pself->DataPtr++ = PREPARE_DELIMIT_WHITECHL;
   //   pself->ColPos++; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   // To je urcite blbe. DataPtr sis spletl s ScriptCharPtr 

      #ifdef __ALLOW_DEBUG_DUMP
        printf(" Act char:%c", *pself->ScriptCharPtr);
      #endif
  
      prepare_solve_whitechar_raw(pself, pself->ScriptCharPtr);
      white_char = prepare_get_whitechar(pself);  
      *pself->DataPtr++ = (char)white_char;
      *pself->DataPtr++ = '\0';

      #ifdef __ALLOW_DEBUG_DUMP
        printf(" White char:%d", white_char);
      #endif

      if (white_char != PREPARE_ERROR_VALUE) {
        if (*pself->ScriptCharPtr != PREPARE_DELIMIT_WHITECHR) {
          prepare_raise_error(pself, PREPARE_RULE_ERR_STATICTEXT4);
        }
        else {
          pself->ScriptCharPtr++;
          pself->ColPos++;
        }
      }
      else {
        prepare_raise_error(pself, PREPARE_RULE_ERR_STATICTEXT5);
      }
    }
  }
}

/*******************************************************************************
 * Funkce analyzuje syntax rule VARIABLETEXT  Varianty: $i $t $24n $<i $<t $<24n
 * Pro novy typ by to muselo byt $c',' nebo $c<LF>
 *
 */
void prepare_solve_variabletext(prepare_main_t * pself)
{
  prepare_data_variabletext_t * variabletext_ptr;
  unsigned short chrscale;
  prepare_txvarib_e txtype;
  int white_char;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" VARIABLETEXT: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  variabletext_ptr = (prepare_data_variabletext_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_variabletext_t);

  variabletext_ptr->Reverser = prepare_solve_reverse(pself);

  chrscale = prepare_solve_chrscale(pself, PREPARE_RULE_ERR_VARTEXT1);
  if (chrscale != PREPARE_STATUS_ERR) {
    variabletext_ptr->ChrScale = chrscale;
  }

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" %c ", *pself->ScriptCharPtr);
  #endif

  *pself->RawTextPtr++ = *pself->ScriptCharPtr;
  txtype = (prepare_txvarib_e)*pself->ScriptCharPtr;

  switch (txtype) {
    case PREPARE_TXVARIB_IDENTIFER:
    case PREPARE_TXVARIB_QUOTED:
      variabletext_ptr->Type = txtype;
      pself->ScriptCharPtr++;
      pself->ColPos++;
    break;

    case PREPARE_TXVARIB_CHENDED:
      pself->ScriptCharPtr++;
      pself->ColPos++;
      *pself->RawTextPtr++ = *pself->ScriptCharPtr;
  
      if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
        pself->ScriptCharPtr++;
        pself->ColPos++;
        *pself->RawTextPtr++ = *pself->ScriptCharPtr;
        variabletext_ptr->Type = PREPARE_TXVARIB_CHENDED;
        variabletext_ptr->Separator = *pself->ScriptCharPtr;
        pself->ScriptCharPtr++;
        pself->ColPos++;
        *pself->RawTextPtr++ = *pself->ScriptCharPtr;
        if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
          pself->ScriptCharPtr++;
          pself->ColPos++;
        }
        else {
          prepare_raise_error(pself, PREPARE_RULE_ERR_VARTEXT2);
        }
      }
      else {
        if (*pself->ScriptCharPtr == PREPARE_DELIMIT_WHITECHL) {
          pself->ScriptCharPtr++;
          pself->ColPos++;
      
          prepare_solve_whitechar_raw(pself, pself->ScriptCharPtr);
          white_char = prepare_get_whitechar(pself);

          #ifdef __ALLOW_DEBUG_DUMP
            printf(" White char:%d", white_char);
          #endif

          if (white_char != PREPARE_ERROR_VALUE) {
            if (*pself->ScriptCharPtr != PREPARE_DELIMIT_WHITECHR) {
              prepare_raise_error(pself, PREPARE_RULE_ERR_VARTEXT3);
            }
            else {
              pself->ScriptCharPtr++;
              pself->ColPos++;
              variabletext_ptr->Type = PREPARE_TXVARIB_WHENDED;
              variabletext_ptr->Separator = (char)white_char;
            }
          }
          else {
            prepare_raise_error(pself, PREPARE_RULE_ERR_VARTEXT4);
          }
        }
      }
    break;

    default:
      prepare_raise_error(pself, PREPARE_RULE_ERR_VARTEXT5);
    break;
  }
}

/*******************************************************************************
 * Funkce analyzuje syntax rule SYNCTEXT Varianty: ?"text1" ?25"text1" ?<"text1" ?<25"text1"
 * Varianta s bilym znakm - ?<CR> ?<<CR>  ?25<CR> ?<25<CR>     
 * pself->RawTextPtr ma na odchodu zapsanou levou zavorku
 */
void prepare_solve_synctext(prepare_main_t * pself)
{
  unsigned short i;
  prepare_data_synctext_t * synctext_ptr;
  unsigned short chrscale;
  int white_char;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" SYNCTEXT: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  synctext_ptr = (prepare_data_synctext_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_synctext_t);

  synctext_ptr->TextIdx = (unsigned short)(pself->DataPtr - pself->DataTab);

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" prvni:%c ", *pself->ScriptCharPtr);
  #endif

  if (*pself->ScriptCharPtr == PREPARE_SYMBOL_REVERSER) {
    if (*pself->ScriptCharPtr == PREPARE_DELIMIT_WHITECHL) {
      *pself->RawTextPtr++ = *pself->ScriptCharPtr;
      pself->ScriptCharPtr++;

      #ifdef __ALLOW_DEBUG_DUMP
        printf(" reverse druhy:%c ", *pself->ScriptCharPtr);
      #endif

      chrscale = prepare_solve_chrscale(pself, PREPARE_RULE_ERR_SYNCTEXT1);
      if (chrscale != PREPARE_STATUS_ERR) {
        synctext_ptr->ChrScale = chrscale;

        #ifdef __ALLOW_DEBUG_DUMP
          printf(" je scale:%d ", chrscale);
        #endif
      }

      if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
        synctext_ptr->Reverser = PREPARE_SYMBOL_REVERSER;
        synctext_ptr->Type = PREPARE_TXSTATIC_NORMAL;
        *pself->RawTextPtr++ = *pself->ScriptCharPtr;
      
        #ifdef __ALLOW_DEBUG_DUMP
          printf(" reverse normal ");
        #endif
      }
      else {
        if (*pself->ScriptCharPtr == PREPARE_DELIMIT_WHITECHL) {
          *pself->RawTextPtr++ = *pself->ScriptCharPtr;
          pself->ScriptCharPtr++;
          synctext_ptr->Reverser = PREPARE_SYMBOL_REVERSER;
          synctext_ptr->Type = PREPARE_TXSTATIC_WHITE;
        }
        else {
          synctext_ptr->Reverser = PREPARE_SYMBOL_NATURALSPACE;
          synctext_ptr->Type = PREPARE_TXSTATIC_WHITE;
        }
      }
    }
    else {
      synctext_ptr->Type = PREPARE_TXSTATIC_ERROR;
      prepare_raise_error(pself, PREPARE_RULE_ERR_SYNCTEXT2);
    }
  }
  else {
    chrscale = prepare_solve_chrscale(pself, PREPARE_RULE_ERR_SYNCTEXT1);
    if (chrscale != PREPARE_STATUS_ERR) {
      synctext_ptr->ChrScale = chrscale;

      #ifdef __ALLOW_DEBUG_DUMP
        printf(" neni reverse je scale:%d ", chrscale);
      #endif
    }

    synctext_ptr->Reverser = PREPARE_SYMBOL_NATURALSPACE;
    *pself->RawTextPtr++ = *pself->ScriptCharPtr;
    if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
      synctext_ptr->Type = PREPARE_TXSTATIC_NORMAL;
    }
    else {
      if (*pself->ScriptCharPtr == PREPARE_DELIMIT_WHITECHL) {
        pself->ScriptCharPtr++;
        synctext_ptr->Type = PREPARE_TXSTATIC_WHITE;
      }
      else {
        synctext_ptr->Type = PREPARE_TXSTATIC_ERROR;
        prepare_raise_error(pself, PREPARE_RULE_ERR_SYNCTEXT3);
      }
    }
  }

  #ifdef __ALLOW_DEBUG_DUMP
    if (synctext_ptr->Reverser == PREPARE_SYMBOL_REVERSER) {
      printf(" REVERSE ");
    }
    if (synctext_ptr->ChrScale > 0) {
      printf(" Scale:%d ", synctext_ptr->ChrScale);
    }
  #endif

  if (synctext_ptr->Type == PREPARE_TXSTATIC_NORMAL) {
    *pself->DataPtr++ = *pself->ScriptCharPtr;
    for (i = 0; i < PREPARE_MAXLEN_STATICTEXT; i++) {
      #ifdef __ALLOW_DEBUG_DUMP
        printf("%c", *pself->ScriptCharPtr);
      #endif
      pself->ScriptCharPtr++;
      pself->ColPos++;
      *pself->DataPtr++ = *pself->ScriptCharPtr;
      *pself->RawTextPtr++ = *pself->ScriptCharPtr;
      if (*pself->ScriptCharPtr == PREPARE_DELIMIT_STATICTX) {
        *pself->DataPtr++ = '\0';
        pself->ScriptCharPtr++;
        pself->ColPos++;
        break;
      }
      if (*pself->ScriptCharPtr == PREPARE_SYMBOL_ENDSCRIPT) {
        prepare_raise_error(pself, PREPARE_RULE_ERR_SYNCTEXT4);
        break;
      }
    }
  }
  else {
    if (synctext_ptr->Type == PREPARE_TXSTATIC_WHITE) {
      #ifdef __ALLOW_DEBUG_DUMP
        printf(" Act char:%c", *pself->ScriptCharPtr);
      #endif
 
      prepare_solve_whitechar_raw(pself, pself->ScriptCharPtr);
      white_char = prepare_get_whitechar(pself);
      *pself->DataPtr++ = (char)white_char;
      *pself->DataPtr++ = '\0';

      #ifdef __ALLOW_DEBUG_DUMP
        printf(" White char:%d", white_char);
      #endif

      if (white_char != PREPARE_ERROR_VALUE) {
        if (*pself->ScriptCharPtr != PREPARE_DELIMIT_WHITECHR) {
          prepare_raise_error(pself, PREPARE_RULE_ERR_SYNCTEXT5);
        }
        else {
          pself->ScriptCharPtr++;
          pself->ColPos++;
          #ifdef __ALLOW_DEBUG_DUMP
            printf(" Posledni po bilym:%c", *pself->ScriptCharPtr);
          #endif
        }
      }
      else {
        prepare_raise_error(pself, PREPARE_RULE_ERR_SYNCTEXT6);
      }
    }
  }
}

/*******************************************************************************
 * Funkce analyzuje syntax rule VARIABLENUMBER  Varianty: #bu #wn #dx #ff #25bu #<25bu
 *
 */
void prepare_solve_variablenumber(prepare_main_t * pself)
{
  prepare_data_variablenumber_t * variablenumber_ptr;
  ascanf_status_e ascanf_status;
  unsigned short chrscale;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" VARNUMBER: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  variablenumber_ptr = (prepare_data_variablenumber_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_variablenumber_t);

  variablenumber_ptr->Reverser = prepare_solve_reverse(pself);

  chrscale = prepare_solve_chrscale(pself, PREPARE_RULE_ERR_VARNUMBER1);
  if (chrscale != PREPARE_STATUS_ERR) {
    variablenumber_ptr->ChrScale = chrscale;
  }

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" Velikost:%c", *pself->ScriptCharPtr);
  #endif
  variablenumber_ptr->Type = (pvals_type_e)*pself->ScriptCharPtr;
  *pself->RawTextPtr++ = *pself->ScriptCharPtr;
  pself->ScriptCharPtr++;
  pself->ColPos++;
  ascanf_status = ascanf_valid_nformat((ascanf_nformat_e)*pself->ScriptCharPtr);

  if (ascanf_status == ASCANF_STATUS_ERRUSER) {
    prepare_raise_error(pself, PREPARE_RULE_ERR_VARNUMBER3);
  }
  else {
    #ifdef __ALLOW_DEBUG_DUMP
      printf(" Format:%c ", *pself->ScriptCharPtr);
    #endif
    variablenumber_ptr->Format = *pself->ScriptCharPtr;
    *pself->RawTextPtr++ = *pself->ScriptCharPtr;
    pself->ScriptCharPtr++;
    pself->ColPos++;
  }
}

/*******************************************************************************
 * Funkce analyzuje syntax rule SKIPTEXT  Varianty: +25 +<25
 *   
 * Uvazuju nekolik dalsich funkci - na zacatek slova, na konec slova, na numericky
 * udaj na slovni udaj, na znakovy udaj 
 * numeric, charakter, symbol, white
 *
 *
 */
void prepare_solve_skiptext(prepare_main_t * pself)
{
  prepare_data_skiptext_t * skiptext_ptr;
  unsigned short chrscale;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" SKIPTEXT: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  skiptext_ptr = (prepare_data_skiptext_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_skiptext_t);

  skiptext_ptr->Reverser = prepare_solve_reverse(pself);
                        
  chrscale = prepare_solve_chrscale(pself, PREPARE_RULE_ERR_SKIPTEXT);
  if (chrscale != PREPARE_STATUS_ERR) {
    skiptext_ptr->ChrScale = chrscale;
  }
      
  if (prepare_skiptext_type(*pself->ScriptCharPtr) == PREPARE_STATUS_OK) {
    skiptext_ptr->Type = *pself->ScriptCharPtr;
    pself->ScriptCharPtr++;
    pself->ColPos++;
  }
  else {  
    skiptext_ptr->Type = PREPARE_SKIPTYPE_EVERYTHING; 
  }
}

/*******************************************************************************
 * Funkce
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!
 * chybi dodelat odpocitani z levelu
 *
 *
 */
void prepare_solve_endblock(prepare_main_t * pself)
{
  #ifdef __ALLOW_DEBUG_DUMP
    printf(" ENDBLOCK: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  pself->DataPtr += sizeof(prepare_data_endblock_t);    
  
  *pself->RawTextPtr++ = '\0';
}

/*******************************************************************************
 * Funkce
 *
 *
 */
void prepare_solve_task(prepare_main_t * pself)
{
  prepare_data_calltask_t * calltask_ptr;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" CALL TASK: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  calltask_ptr = (prepare_data_calltask_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_calltask_t);

  prepare_scanf_number(pself);
  if (pself->ScanfSession.RStatus < 0) {
    prepare_raise_error(pself, PREPARE_RULE_ERR_CALL);
  }
  else {
    #ifdef __ALLOW_DEBUG_DUMP
      printf("%d Err:%d", pself->ScanfSession.Work.Val32, pself->ScanfSession.RStatus);
    #endif
    calltask_ptr->Number = pself->ScanfSession.Work.Val32;
    pself->ScriptCharPtr += pself->ScanfSession.RStatus;
    pself->ColPos += pself->ScanfSession.RStatus;
    
    itoa(pself->ScanfSession.Work.Val32, pself->RawTextPtr, 10); 
    pself->RawTextPtr += pself->ScanfSession.RStatus;
  }
  if (calltask_ptr->Number == 0) {
    prepare_raise_error(pself, PREPARE_RULE_ERR_CALL);
  }
}

/*******************************************************************************
 * Funkce
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * chybi dodelat level
 *
 *
 */
void prepare_solve_macro(prepare_main_t * pself)
{
  prepare_data_macro_t * macro_ptr;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" CALL MACRO: ");
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  macro_ptr = (prepare_data_macro_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_macro_t);

  prepare_scanf_number(pself);
  if (pself->ScanfSession.RStatus < 0) {
    prepare_raise_error(pself, PREPARE_RULE_ERR_MACRO1);
  }
  else {
    #ifdef __ALLOW_DEBUG_DUMP
      printf("%d ", pself->ScanfSession.Work.Val32);
    #endif
    macro_ptr->Id = pself->ScanfSession.Work.Val32;
    macro_ptr->SaveRow = pself->RowPos;
    macro_ptr->SaveCol = pself->ColPos;

    pself->ScriptCharPtr += pself->ScanfSession.RStatus;
    pself->ColPos += pself->ScanfSession.RStatus;
    
    itoa(pself->ScanfSession.Work.Val32, pself->RawTextPtr, 10); 
    pself->RawTextPtr += pself->ScanfSession.RStatus;
  }
  if (macro_ptr->Id == 0) {
    prepare_raise_error(pself, PREPARE_RULE_ERR_MACRO2);
  }
}

/*******************************************************************************
 * Funkce vytvori prostor pro strukturu a zkontroluje Level.
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
 * Ale pozor, je zde systemova chyba. Makra se tohoto systemu levelu
 * taky ucastni a nejsou zde naprosto zohlednena. Jde zde vyhradne o zavorku.
 */
void prepare_solve_lbracket(prepare_main_t * pself, char lbracket)
{
  prepare_data_lbracket_t * lbracket_ptr;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" LBRACKET: %c", lbracket);
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  lbracket_ptr = (prepare_data_lbracket_t *)pself->RuleTab[pself->RuleAct].StructPtr;
  pself->DataPtr += sizeof(prepare_data_lbracket_t);

  if (++pself->LevelAct < PREPARE_LNG_LEVELTAB) {
    pself->LevelTab[pself->LevelAct] = lbracket;
    lbracket_ptr->BeginIdx = 0;
    #ifdef __ALLOW_DEBUG_DUMP
      printf(" New level:%d ", pself->LevelAct);
    #endif
  }
  else {
    prepare_raise_error(pself, PREPARE_RULE_ERR_LBRACKET);
  }
}

/*******************************************************************************
 * Funkce resi pravou zavorku. Dela kontrolu na:
 * -Jestli je parova leva zavorka stejneho druhu
 * -Jestli je znak vubec zavorka
 * -Jestli je spravny level
 * Funkce alokuje misto pro Struct typu ENDBLOCK, protoze v prubehu preskupeni
 * rules se prava zavorka nahrazuje strednikem, ktery vyzaduje ulozeni navratove
 * hodnoty.
 */
void prepare_solve_pbracket(prepare_main_t * pself, char pbracket)
{
  char lbracket;

  #ifdef __ALLOW_DEBUG_DUMP
    printf(" PBRACKET: %c ", pbracket);
  #endif

  pself->RuleTab[pself->RuleAct].StructPtr = (void *)pself->DataPtr;
  pself->DataPtr += sizeof(prepare_data_endblock_t);

  if (pself->LevelAct > 0) {
    lbracket = prepare_invert_bracket(pbracket);
    if (lbracket =='\0') {
      prepare_raise_error(pself, PREPARE_RULE_ERR_PBRACKET1);
    }
    else {
      if (pself->LevelTab[pself->LevelAct] != lbracket) {
        prepare_raise_error(pself, PREPARE_RULE_ERR_PBRACKET2);
        #ifdef __ALLOW_DEBUG_DUMP
          printf(" %c != %c ", pself->LevelTab[pself->LevelAct], lbracket);
        #endif
      }
      else {
        pself->LevelAct--;
        #ifdef __ALLOW_DEBUG_DUMP
          printf(" New level:%d ", pself->LevelAct);
        #endif
      }
    }
  }
  else {
    prepare_raise_error(pself, PREPARE_RULE_ERR_PBRACKET3);
  }
}

/******************************************************************************/
/******************************************************************************/

/*******************************************************************************
 * Funkce slouzi k vytvoreni polozky RuleTab, cili k zalozeni noveho rule.
 * Dale slouzi k vyseparovani a ulozeni pridavnych udaju k nekterym rules.
 * 1. Dela ruzne kontroly.
 * 2. Plni RuleTab[]. Zde vyplni symbol rule a ukazatel na strukturu s hodnotami.
 *    Zde vyplni vrchol datoveho ukazatele DataPtr.
 * 3. Dalsi udaje vyplnuje az v jednotlivych funkcich.
 * 4. Zde inkrementuje RuleAct.
 *
 * RuleAct musi byt inkrementovan az po volani funkci solve(), protoze v nich musi
 * ukazovat na aktualni rule.
 *
 * Vse je ulozeno v DataTab. Jak struktury udaju, tak i retezce, pokud jsou soucasti
 * datove casti. Jako ukazatele se vzdy uklada DataPtr, ktery ukazuje na vrch datove
 * pameti.
 *
 * Jestli chces pridat nove rule, musis ho taky pridat do tabulek class_pair_tab[] a
 * Prepare_ClErrorTab[].
 */
void prepare_area_inirule(prepare_main_t * pself)
{
  char script_char;
  prepare_errors_e class_result;
  prepare_clerror_t cl_error;

  #ifdef __ALLOW_DEBUG_DUMP
    prepare_main_dump(pself, "  ");
  #endif

  if (pself->RuleAct >= PREPARE_LNG_RULETAB) {
    prepare_raise_error(pself, PREPARE_RULE_ERR_SOLVE1);
    return;
  }

  script_char = *pself->ScriptCharPtr;
  cl_error = prepare_get_clerror(script_char);
  if (cl_error.ClassSymbol == PREPARE_ERROR_VALUE) {
    prepare_raise_error(pself, PREPARE_RULE_ERR_SOLVE2);
    #ifdef __ALLOW_DEBUG_DUMP
      printf(" Symb:%d, Type:%d ", cl_error.ClassSymbol, cl_error.ClassType);
    #endif
    return;
  }

  class_result = prepare_check_class(&pself->LastClass, cl_error.ClassType);
  if ( class_result != PREPARE_CLASS_ERR_NOERROR) {
    prepare_raise_error(pself, class_result);  //!!!!!!! jeste class_result reportovat do chyby
    return;
  }

  pself->RuleTab[pself->RuleAct].Symbol = script_char;
  pself->RuleTab[pself->RuleAct].Order = pself->RuleAct;
                                   
  *pself->RawTextPtr++ = ' ';
  pself->RawPtrTab[pself->RuleAct] = pself->RawTextPtr;
  *pself->RawTextPtr++ = script_char;
  *pself->RawTextPtr = '\0';  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
                    
  pself->ScriptCharPtr++;
  pself->ColPos++;

  switch (script_char) {
    case PREPARE_SYMBOL_ANYSPACE      : prepare_solve_anyspace(pself);                   break; // '-'
    case PREPARE_SYMBOL_ONEORMORESPACE: prepare_solve_oneormorespace(pself);             break; // '='
    case PREPARE_SYMBOL_STATICTEXT    : prepare_solve_statictext(pself);                 break; // '%'
    case PREPARE_SYMBOL_VARIABLETEXT  : prepare_solve_variabletext(pself);               break; // '$'
    case PREPARE_SYMBOL_SYNCTEXT      : prepare_solve_synctext(pself);                   break; // '?'
    case PREPARE_SYMBOL_VARIABLENUMBER: prepare_solve_variablenumber(pself);             break; // '#'
    case PREPARE_SYMBOL_SKIPTEXT      : prepare_solve_skiptext(pself);                   break; // '+'
    case PREPARE_SYMBOL_OR            : pself->RuleTab[pself->RuleAct].StructPtr = NULL; break; // '|'
    case PREPARE_SYMBOL_AND           : pself->RuleTab[pself->RuleAct].StructPtr = NULL; break; // '.'
    case PREPARE_SYMBOL_ENDBLOCK      : prepare_solve_endblock(pself);                   break; // ';'
    case PREPARE_SYMBOL_INITVALS      : pself->RuleTab[pself->RuleAct].StructPtr = NULL; break; // '&'
    case PREPARE_SYMBOL_CALLTASK      : prepare_solve_task(pself);                       break; // '!'
    case PREPARE_SYMBOL_MACRO         : prepare_solve_macro(pself);                      break; // '*'
    case PREPARE_SYMBOL_LBRACKET      :
    case PREPARE_SYMBOL_LOPTION       :
    case PREPARE_SYMBOL_LREPEAT       : prepare_solve_lbracket(pself, script_char);      break; // '([{'
    case PREPARE_SYMBOL_PBRACKET      :
    case PREPARE_SYMBOL_POPTION       :
    case PREPARE_SYMBOL_PREPEAT       : prepare_solve_pbracket(pself, script_char);      break; // ')]}'
  }
  #ifdef __ALLOW_DEBUG_DUMP
    printf(" %p ", pself->RuleTab[pself->RuleAct].StructPtr);
  #endif
  pself->RuleAct++;
}

/*******************************************************************************
 * Funkce kontroluje syntax uvodni hlavicky makra. Priklad: Syntax je: *5:
 * * uz je zkontrolovana. Prevede se Id makra do binarni podoby a zkontroluje
 * se na meze. Nesmi byt 0. 0 je hlavni script. Pak se zkontroluje ta :
 * Vytvori se polozka makra v tabulce MacroTab[]
 */
void prepare_area_inimacro(prepare_main_t * pself)
{
  unsigned short i;

  pself->ScriptCharPtr++;
  pself->ColPos++;
  prepare_scanf_number(pself);
  if (pself->ScanfSession.RStatus < 0) {
    prepare_raise_error(pself, PREPARE_AREA_ERR_INIMACRO1);
  }
  else {
    pself->ScriptCharPtr += pself->ScanfSession.RStatus;
    pself->ColPos += pself->ScanfSession.RStatus;
    prepare_skip_spaces(pself);
    if (*pself->ScriptCharPtr != ':') {
      prepare_raise_error(pself, PREPARE_AREA_ERR_INIMACRO2);
    }
    else {
      if (pself->ScanfSession.Work.Val32 == 0) {
        prepare_raise_error(pself, PREPARE_AREA_ERR_INIMACRO3);
      }
      else {
        if (pself->MacroCnt >= PREPARE_LNG_MACROTAB) {
          prepare_raise_error(pself, PREPARE_AREA_ERR_INIMACRO4);
        }
        else {
          for (i = 0; i < pself->MacroCnt; i++) {
            if (pself->MacroTab[i].Id == pself->ScanfSession.Work.Val32) {
              pself->ScriptCharPtr++;
              pself->ColPos++;
              prepare_raise_error(pself, PREPARE_AREA_ERR_INIMACRO5);
              return;
            }
          }
          pself->MacroTab[pself->MacroCnt].Id = pself->ScanfSession.Work.Val32;
          pself->MacroCnt++;
          pself->ScriptCharPtr++;
          pself->ColPos++;
          if (pself->MacroCnt >= PREPARE_LNG_MACROTAB) {
            prepare_raise_error(pself, PREPARE_AREA_ERR_INIMACRO6);
          }
          #ifdef __ALLOW_DEBUG_DUMP
            printf(":%d, MacroCnt:%d", pself->ScanfSession.Work.Val32, pself->MacroCnt);
          #endif
        }
      }
    }
  }
}

/*******************************************************************************
 *Funkce
 */
void prepare_area_endscript(prepare_main_t * pself)
{
  if (pself->LevelAct != 0) {
    prepare_raise_error(pself, PREPARE_AREA_ERR_ENDSCRIPT);
  }
  else {
    #ifdef __ALLOW_DEBUG_DUMP
      printf("\r\n\r\n---------------------------------------------------------------");
      printf("\r\nZAKLADNI PARSING V PORADKU DOKONCEN");
      printf("\r\n---------------------------------------------------------------\r\n");
    #endif
    pself->ScriptArea = PREPARE_AREA_IDLE;
  }
}

/*******************************************************************************
 *Funkce
 */
void prepare_syntax_analysis_processing(prepare_main_t * pself)
{
  static unsigned char return_area;

  switch (pself->ScriptArea) {
    case PREPARE_AREA_IDLE:
    break;

    case PREPARE_AREA_RULES:
      switch (*pself->ScriptCharPtr) {
        case PREPARE_SYMBOL_NATURALSPACE:
          pself->ScriptCharPtr++;
          pself->ColPos++;
        break;
        case PREPARE_SYMBOL_ENDROWLF:
        case PREPARE_SYMBOL_ENDROWCR:
          prepare_raise_error(pself, PREPARE_AREA_ERR_RULECRLF);
        break;
        case PREPARE_SYMBOL_ENDBLOCK:
          prepare_area_inirule(pself);
          if (pself->LevelAct != 0) {
            prepare_raise_error(pself, PREPARE_AREA_ERR_RULELEVEL);
          }
          else {
            #ifdef __ALLOW_DEBUG_DUMP
              printf(" Konec bloku v RULES, nyni AF\r\n");
            #endif
            pself->ScriptMode = PREPARE_MODE_MACRORULES;
            pself->ScriptArea = PREPARE_AREA_AFTEREND;
          }
        break;
        case PREPARE_SYMBOL_COMMENT:
          prepare_raise_error(pself, PREPARE_AREA_ERR_RULECOMM);
        break;
        case PREPARE_SYMBOL_COUPLER:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "C1");
          #endif
          pself->ScriptCharPtr++;
          pself->ColPos++;
          pself->ScriptArea = PREPARE_AREA_COUPLER1;
        break;
        case PREPARE_SYMBOL_ENDSCRIPT:
          prepare_raise_error(pself, PREPARE_AREA_ERR_RULEEND);
        break;
        default:
          prepare_area_inirule(pself);
        break;
      }
    break;

    case PREPARE_AREA_AFTEREND:
      switch (*pself->ScriptCharPtr) {
        case PREPARE_SYMBOL_NATURALSPACE:
          pself->ScriptCharPtr++;
          pself->ColPos++;
        break;
        case PREPARE_SYMBOL_ENDROWLF:
          #ifdef __ALLOW_DEBUG_DUMP
            printf("LF ");
          #endif
          pself->ScriptCharPtr++;
          pself->ColPos = 0;
          pself->RowPos++;
        break;
        case PREPARE_SYMBOL_ENDROWCR:
          #ifdef __ALLOW_DEBUG_DUMP
            printf("CR ");
          #endif
          pself->ScriptCharPtr++;
          pself->ColPos++;
        break;
        case PREPARE_SYMBOL_ENDBLOCK:
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // misto chyby mozna jen ukoncit
          prepare_raise_error(pself, PREPARE_AREA_ERR_AFTERMACRO);
        break;
        case PREPARE_SYMBOL_COMMENT:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "CM");
          #endif
          pself->ScriptCharPtr++;
          pself->ColPos++;
          return_area = PREPARE_AREA_AFTEREND;
          pself->ScriptArea = PREPARE_AREA_COMMENT;
        break;
        case PREPARE_SYMBOL_COUPLER:
          prepare_raise_error(pself, PREPARE_AREA_ERR_AFTERCOUPL);
        break;
        case PREPARE_SYMBOL_ENDSCRIPT:
          switch(pself->ScriptMode) {
            case PREPARE_MODE_PRIMARYRULES: prepare_raise_error(pself, PREPARE_AREA_ERR_AFTEREND); break;
            case PREPARE_MODE_MACRORULES:   prepare_area_endscript(pself); break;
          }
        break;
        case PREPARE_SYMBOL_MACRO:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "RU");
            printf(" Zacatek makra");
          #endif
          pself->ScriptArea = PREPARE_AREA_RULES;
          switch(pself->ScriptMode) {
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            case PREPARE_MODE_PRIMARYRULES: if (pself->MacroCnt != 0) prepare_raise_error(pself, PREPARE_AREA_ERR_INIPRIMARY); break;
            case PREPARE_MODE_MACRORULES:   prepare_area_inimacro(pself); break;
          }
        break;
        default:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "RU");
            printf(" Ostatni znaky");
          #endif
          if (pself->ScriptMode == PREPARE_MODE_PRIMARYRULES) {
            pself->ScriptArea = PREPARE_AREA_RULES;
          }
          else {
            prepare_raise_error(pself, PREPARE_AREA_ERR_INIPRIMARY);
          }
          return;

          pself->ScriptArea = PREPARE_AREA_RULES;
          if (pself->ScriptMode == PREPARE_MODE_PRIMARYRULES) {
            if (pself->MacroCnt != 0) {
              prepare_raise_error(pself, PREPARE_AREA_ERR_INIPRIMARY);
            }
          }
        break;
      }
    break;

    case PREPARE_AREA_COUPLER1:
      switch (*pself->ScriptCharPtr) {
        case PREPARE_SYMBOL_NATURALSPACE:
          pself->ScriptCharPtr++;
          pself->ColPos++;
        break;
        case PREPARE_SYMBOL_ENDROWLF:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "C2");
            printf(" z C1-LF");
          #endif
          pself->ScriptArea = PREPARE_AREA_COUPLER2;
        break;
        case PREPARE_SYMBOL_ENDROWCR:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "C2");
            printf(" z C1-CR");
          #endif
          pself->ScriptArea = PREPARE_AREA_COUPLER2;
        break;
        case PREPARE_SYMBOL_COMMENT:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "CM");
            printf(" z C1");
          #endif
          pself->ScriptCharPtr++;
          pself->ColPos++;
          return_area = PREPARE_AREA_COUPLER2;
          pself->ScriptArea = PREPARE_AREA_COMMENT;
        break;
        default:
          prepare_raise_error(pself, PREPARE_AREA_ERR_COUPLER1);
        break;
      }
    break;

    case PREPARE_AREA_COUPLER2:
      switch (*pself->ScriptCharPtr) {
        case PREPARE_SYMBOL_NATURALSPACE:
          pself->ScriptCharPtr++;
          pself->ColPos++;
        break;
        case PREPARE_SYMBOL_ENDROWLF:
          pself->ScriptCharPtr++;
          pself->ColPos = 0;
          pself->RowPos++;
        break;
        case PREPARE_SYMBOL_ENDROWCR:
          pself->ScriptCharPtr++;
          pself->ColPos++;
        break;
        case PREPARE_SYMBOL_COMMENT:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "CM");
            printf(" z C2");
          #endif
          pself->ScriptCharPtr++;
          pself->ColPos++;
          return_area = PREPARE_AREA_COUPLER2;
          pself->ScriptArea = PREPARE_AREA_COMMENT;
        break;
        case PREPARE_SYMBOL_ENDBLOCK:
        case PREPARE_SYMBOL_COUPLER:
        case PREPARE_SYMBOL_ENDSCRIPT:
          prepare_raise_error(pself, PREPARE_AREA_ERR_COUPLER2);
        break;
        default:
          #ifdef __ALLOW_DEBUG_DUMP
            prepare_main_dump(pself, "RU");
            printf(" z C2");
          #endif
          pself->ScriptArea = PREPARE_AREA_RULES;
        break;
      }
    break;

    case PREPARE_AREA_COMMENT:
      switch (*pself->ScriptCharPtr) {
        case PREPARE_SYMBOL_ENDROWLF:
          pself->ScriptArea = return_area;
          #ifdef __ALLOW_DEBUG_DUMP
            if (return_area == PREPARE_AREA_AFTEREND) {
              prepare_main_dump(pself, "AF");
              printf(" z CM-LF");
            }
            if (return_area == PREPARE_AREA_COUPLER2) {
              prepare_main_dump(pself, "C2");
              printf(" z CM-LF");
            }
          #endif
        break;
        case PREPARE_SYMBOL_ENDROWCR:
          pself->ScriptArea = return_area;
          #ifdef __ALLOW_DEBUG_DUMP
            if (return_area == PREPARE_AREA_AFTEREND) {
              prepare_main_dump(pself, "AF");
              printf(" z CM-CR");
            }
            if (return_area == PREPARE_AREA_COUPLER2) {
              prepare_main_dump(pself, "C2");
              printf(" z CM-CR");
            }
          #endif
        break;
        case PREPARE_SYMBOL_ENDSCRIPT:
          if (return_area == PREPARE_AREA_AFTEREND) {
            #ifdef __ALLOW_DEBUG_DUMP
              printf(" z CM ");
            #endif
            prepare_area_endscript(pself);
          }
          else {
            prepare_raise_error(pself, PREPARE_AREA_ERR_COMMENT);
          }
        break;
        default:
          pself->ScriptCharPtr++;
          pself->ColPos++;
        break;
      }
    break;

    case PREPARE_AREA_ERROR:
    break;

    default:
      prepare_raise_error(pself, PREPARE_AREA_ERR_GLOBAL);
    break;
  }
}

/*******************************************************************************
 * Funkce vytvori vysledny kod.


 *
 */

prepare_status_e prepare_make_pcode(prepare_main_t * pself)
{
  prepare_mode_make_e make_state;
  prepare_data_lbracket_t * lbracket_ptr;
  prepare_data_endblock_t * endblock_ptr;
  void * struct_ptr;
  unsigned short i, irule_rdsav, irule_rdend, icode_wrend, icode_rdself, icode_retidx, imacro_wract, cpself_stop, ruleord;
  int blevel; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! zmenit
  char char_code, char_self, char_skip, pbracket, boccured;

  irule_rdend = 0;
  icode_wrend = 0;
  icode_rdself = 0;
  imacro_wract = 0;
  make_state = PREPARE_MODE_MAKE_COPYRULE;

  #ifdef __ALLOW_DEBUG_DUMP
    printf("\r\n---------------------------------------------------------------");
    printf("\r\nZACATEK PREUSPORADANI PODLE HLOUBKY VNORENI");
    printf("\r\n---------------------------------------------------------------");
    printf("\r\n  csymbol, irule_rdend, icode_wrend");
    printf("\r\n---------------------------------------------------------------\r\n");
  #endif

  //for (i = 0; i < PREPARE_LNG_RULETAB; i++) { !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  for (i = 0; i < 1000; i++) {
    switch (make_state) {
      case PREPARE_MODE_MAKE_IDLE:
      break;

      case PREPARE_MODE_MAKE_COPYRULE: // Z RuleTab cte vnitrek zavorky a kopiruje do CodeTab az po ENDBLOCK.
        // Jakmile je leva zavorka, jde na preskakovani vnitrku zavorky.
        // Pokud je ENDBLOCK, kopirovani v tomto state skoncilo a jde se na COPYSELF.
        // Nutne hodnoty:
        // - char_code   - Znak rule, kt. se hodnoti.
        // - irule_rdend - nastavuje se ve pouze na zacatku programu. Cte se z ni pouze v tomto state.
        // - icode_wrend - nijak se nenastavuje. Do CodeTab se zapisuje porad na konec.
        // - cpself_stop - jakmile narazi na BLOCKEND, tak tam da obsah icode_wrend a skoci na COPYSELF.
        // - blevel      - levely se pricitaji v rezimu RULESKIP, pocitaji vnoreni zavorky, aby se vratil na tu spravnou.
        // - imacro_wract- index makra. Jde o index jeho zacatku, aby se rozkopiroval do vsech volani makra.
        // - lbracket_ptr
        char_code  = pself->RuleTab[irule_rdend].Symbol;
        ruleord    = pself->RuleTab[irule_rdend].Order;
        struct_ptr = pself->RuleTab[irule_rdend].StructPtr;

        #ifdef __ALLOW_DEBUG_DUMP
          printf(" CR:  %c %03u --> %03u ", char_code, irule_rdend, icode_wrend);
        #endif

        pself->CodeTab[icode_wrend].Symbol    = char_code;
        pself->CodeTab[icode_wrend].Order     = ruleord;
        pself->CodeTab[icode_wrend].StructPtr = struct_ptr;

        switch (char_code) {
          case PREPARE_SYMBOL_LBRACKET:   // '('
          case PREPARE_SYMBOL_LOPTION :   // '['
          case PREPARE_SYMBOL_LREPEAT :   // '{'
            irule_rdend++;
            icode_wrend++;
            lbracket_ptr = (prepare_data_lbracket_t *)struct_ptr;
            lbracket_ptr->BeginIdx = irule_rdend;
            blevel = 0;
            make_state = PREPARE_MODE_MAKE_SKIPRULE;
            #ifdef __ALLOW_DEBUG_DUMP
              printf(" -> SKIPRULE RuleIdx = %u \r\n", irule_rdend);
            #endif
          break;
          case PREPARE_SYMBOL_ENDBLOCK: // Vyhradne za hlavnim scriptem nebo za hlavnim makrem
            cpself_stop = icode_wrend;
            boccured = 0;
            irule_rdend++;
            icode_wrend++;
            make_state = PREPARE_MODE_MAKE_COPYSELF;
            #ifdef __ALLOW_DEBUG_DUMP
              printf(" ->COPYSELF: stop = wrend:%u oc nulovan\r\n", cpself_stop);
              printf(":::::::::::::::::::::::::::::::::::::::::::::\r\n");
            #endif
          break;
          case PREPARE_SYMBOL_ENDSCRIPT:
            pself->CodeCnt = icode_wrend;
            pself->RuleCnt = irule_rdend;
            pself->DataCnt = (unsigned short)(pself->DataPtr - pself->DataTab);
            make_state = PREPARE_AREA_IDLE;
            #ifdef __ALLOW_DEBUG_DUMP
              printf("\r\n---------------------------------------------------------------");
              printf("\r\nPREUSPORADANI USPESNE DOKONCENO");
              printf("\r\n---------------------------------------------------------------\r\n");
            #endif
            return (PREPARE_STATUS_OK);
          break;
          default:
            irule_rdend++;
            icode_wrend++;
            #ifdef __ALLOW_DEBUG_DUMP
              printf("\r\n");
            #endif
          break;
        }
      break;

      case PREPARE_MODE_MAKE_SKIPRULE: // Preskakovani znaku RuleTab az do prave zavorky
        char_skip = pself->RuleTab[irule_rdend].Symbol;
        #ifdef __ALLOW_DEBUG_DUMP
          switch (blevel) {
            case 0:  printf(" sr:            %c %c   %03u %01u ", char_code, char_skip, irule_rdend, blevel);      break;
            case 1:  printf(" sr:              %c %c   %03u %01u ", char_code, char_skip, irule_rdend, blevel);    break;
            default: printf(" sr:                %c %c   %03u %01u ", char_code, char_skip, irule_rdend, blevel);  break;
          }
        #endif
        irule_rdend++;
        if (char_skip == char_code) {
          blevel++;
        }
        else {
          pbracket = prepare_invert_bracket(char_skip);
          if (pbracket == char_code) {
            if (blevel == 0) {
              make_state = PREPARE_MODE_MAKE_COPYRULE;
              #ifdef __ALLOW_DEBUG_DUMP
                printf(" -> COPYRULE %c %c %c", char_code, char_skip, pbracket);
              #endif
            }
            else {
              blevel--;
            }
          }
        }
        #ifdef __ALLOW_DEBUG_DUMP
          printf("\r\n");
        #endif
      break;

      case PREPARE_MODE_MAKE_COPYSELF: // Z CodeTab cte a kontroluje, jestli neni leva zavorka.
        // Pokud narazi na levou zavorku, vyzvedne z jeji struktury index na vnitrek a jde na state COPYCODE.
        // Pokud narazi na ENDBLOK, tak-pokud je cpself_stop, coz je konec levelu, tak kontroluje boccured.
        // Pokud je boccured 1, tak narazil alespon na 1 zavorku a musi zacit jeji nahrazeni. Pokud ne, jde na dalsi cele makro.
        // Pokud neni boccured, tak ENDBLOCK patri zavorce a neni treba se jim zabyvat.
        // Nutne hodnoty:
        // - char_self    - neni to zde jen zavorka, ale kazdy znak. Pouze kdyz to je zavorka, jde na state COPYCODE.
        // - pbracket     - Ten se vypocte inverzi a pouzije se ve state COPYCODE k navratu.
        // - icode_rdself - Z tohoto indexu cte poporade polozky CodeTab. Jde v zavesu za icode_wrend, vzdy zastavi na cpself_stop.
        // - icode_wrend  - Zde se tento index zapisuje do struktury jako RuleIdx, protoze to je novy index zavorky. Zde se nemeni
        // - irule_rdsav  - index, od kt. se budou z RuleTab kopirovat vnitrky zavorek.
        // - cpself_stop  - jakmile rdself dosahne teto hodnoty, konci jeden level state COPYSELF a
        // - boccured     - jakmile za cele projeti levelu neni ani jedna zavorka, tak konci cele makro a jde na dalsi.
        // - icode_retidx - je index pozice hned za levou zavorkou. Zde se ulozi, dosazuje se do ENDBLOCKu prave zavorky jako navratovy index zavorky.

        char_self = pself->CodeTab[icode_rdself].Symbol;

        #ifdef __ALLOW_DEBUG_DUMP
          printf(" EL:      %c  %03u %03u ", char_self, icode_rdself, icode_wrend);
        #endif

        switch (char_self) {
          case PREPARE_SYMBOL_LBRACKET:   // '('  12
          case PREPARE_SYMBOL_LOPTION :   // '['  13
          case PREPARE_SYMBOL_LREPEAT :   // '{'  14
            lbracket_ptr = (prepare_data_lbracket_t *)pself->CodeTab[icode_rdself].StructPtr;
            irule_rdsav = lbracket_ptr->BeginIdx;
            lbracket_ptr->BeginIdx = icode_wrend;
            icode_rdself++;
            icode_retidx = icode_rdself;
            boccured = 1;
            make_state = PREPARE_MODE_MAKE_COPYCODE;
            #ifdef __ALLOW_DEBUG_DUMP
              printf("LBRACKET - -> COPYCODE %c rdsav = %u wrend = %u oc nastaven\r\n", char_self, irule_rdsav, icode_wrend);
            #endif
          break;
          case PREPARE_SYMBOL_ENDBLOCK:
          case PREPARE_SYMBOL_PBRACKET:
          case PREPARE_SYMBOL_POPTION :
          case PREPARE_SYMBOL_PREPEAT :
            if (icode_rdself == cpself_stop) {
              if (boccured) {
                boccured = 0;
                cpself_stop = icode_wrend - 1;
                #ifdef __ALLOW_DEBUG_DUMP
                  printf("PBRACKET - rdself == stop  symbol=%c nstop=%u, wrend=%u oc nulovan\r\n", char_self, cpself_stop, icode_wrend);
                  printf("===========================================\r\n");
                #endif
              }
              else {
                if (imacro_wract < pself->MacroCnt) {
                  pself->MacroTab[imacro_wract++].BeginIdx = icode_wrend;
                }
                make_state = PREPARE_MODE_MAKE_COPYRULE;
                #ifdef __ALLOW_DEBUG_DUMP
                  printf("PBRACKET - -> COPYRULE, Macro - symbol=%c WrAct:%d Begin:%d oc==0\r\n", char_self, imacro_wract, icode_wrend);
                  printf("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\r\n");
                #endif
              }
            }
            else {
              #ifdef __ALLOW_DEBUG_DUMP
                printf("PBRACKET - symbol=%c  rdself=%u  stop=%u  wrend=%u\r\n", char_self, icode_rdself, cpself_stop, icode_wrend);
              #endif
            }
            icode_rdself++;
          break;
          case PREPARE_SYMBOL_ENDSCRIPT:
            prepare_raise_error(pself, PREPARE_CODE_ERR_COPYSELF);
            pself->CodeCnt = icode_wrend;
            pself->RuleCnt = irule_rdend;
            make_state = PREPARE_AREA_IDLE;
            return (PREPARE_STATUS_ERR);
          break;
          default:
            icode_rdself++;
            #ifdef __ALLOW_DEBUG_DUMP
              printf("ostatni\r\n");
            #endif
          break;
        }
      break;

      case PREPARE_MODE_MAKE_COPYCODE: // Z RuleTab cte vnitrek zavorky a kopiruje do CodeTab az po pravou zavorku.
        // Jakmile je leva zavorka, jde na preskakovani
        // Pokud je prava zavorka, ale stejna na jakou se sem skocilo ze state, tak je konec, dosadi ENDBLOCK a jde dal.
        // Nutne hodnoty:
        // - char_code    - Znak rule, kt. se hodnoti.
        // - irule_rdsav  - nastavuje se ve state COPYSELF ze struktury. Index ze kt. se ctou data zavorky.
        // - icode_wrend  - nijak se nenastavuje. Do CodeTab se zapisuje porad na konec.
        // - icode_retidx - Index CodeTab, na ktery skoci po dokonceni zavorky.
        char_code  = pself->RuleTab[irule_rdsav].Symbol;
        ruleord    = pself->RuleTab[irule_rdsav].Order;
        struct_ptr = pself->RuleTab[irule_rdsav].StructPtr;

        #ifdef __ALLOW_DEBUG_DUMP
          printf(" CC:            %c %03u --> %03u ", char_code, irule_rdsav, icode_wrend);
        #endif

        switch (char_code) {
          case PREPARE_SYMBOL_LBRACKET:   // '('
          case PREPARE_SYMBOL_LOPTION :   // '['
          case PREPARE_SYMBOL_LREPEAT :   // '{'
            pself->CodeTab[icode_wrend].Symbol    = char_code;
            pself->CodeTab[icode_wrend].Order     = ruleord;
            pself->CodeTab[icode_wrend].StructPtr = struct_ptr;

            irule_rdsav++;
            icode_wrend++;

            lbracket_ptr = (prepare_data_lbracket_t *)struct_ptr;
            lbracket_ptr->BeginIdx = irule_rdsav;
            blevel = 0;
            make_state = PREPARE_MODE_MAKE_SKIPCODE;
            #ifdef __ALLOW_DEBUG_DUMP
              printf("LBRACKET -> SKIPCODE RuleIdx = %u \r\n", irule_rdsav);
            #endif
          break;
          case PREPARE_SYMBOL_PBRACKET:   //  )
          case PREPARE_SYMBOL_POPTION :   //  ]
          case PREPARE_SYMBOL_PREPEAT :   //  }
            pbracket = prepare_invert_bracket(char_self);
            if (char_code == pbracket) {
              pself->CodeTab[icode_wrend].Symbol    = char_code;
              pself->CodeTab[icode_wrend].Order     = ruleord;
              pself->CodeTab[icode_wrend].StructPtr = struct_ptr;
              endblock_ptr = (prepare_data_endblock_t *)struct_ptr;
              endblock_ptr->RetIdx = icode_retidx;
              irule_rdsav++;
              icode_wrend++;
              #ifdef __ALLOW_DEBUG_DUMP
                printf("PBRACKET -> COPYSELF %c rdsav=%03u wrend=%03u Added ENDBLOCK\r\n", pself->CodeTab[icode_wrend].Symbol, irule_rdsav, icode_wrend);
                printf("-----------------------------------------\r\n");
              #endif
              make_state = PREPARE_MODE_MAKE_COPYSELF;
            }
            else {
              prepare_raise_error(pself, PREPARE_CODE_ERR_COPYCODE1);
              make_state = PREPARE_AREA_IDLE;
              return (PREPARE_STATUS_ERR);
            }
          break;
          case PREPARE_SYMBOL_ENDBLOCK:
            prepare_raise_error(pself, PREPARE_CODE_ERR_COPYCODE2);
            make_state = PREPARE_AREA_IDLE;
            return (PREPARE_STATUS_ERR);
          break;
          case PREPARE_SYMBOL_ENDSCRIPT:
            prepare_raise_error(pself, PREPARE_CODE_ERR_COPYCODE3);
            make_state = PREPARE_AREA_IDLE;
            return (PREPARE_STATUS_ERR);
          break;
          default:
            pself->CodeTab[icode_wrend].Symbol    = char_code;
            pself->CodeTab[icode_wrend].Order     = ruleord;
            pself->CodeTab[icode_wrend].StructPtr = struct_ptr;

            irule_rdsav++;
            icode_wrend++;
            #ifdef __ALLOW_DEBUG_DUMP
              printf("ostatni\r\n");
            #endif
          break;
        }
      break;

      case PREPARE_MODE_MAKE_SKIPCODE: // Preskakovani znaku RuleTab az do prave zavorky
        char_skip = pself->RuleTab[irule_rdsav].Symbol;
        #ifdef __ALLOW_DEBUG_DUMP
          printf(" sc:                  %c - %c %03u %01u ", char_code, char_skip, irule_rdsav, blevel);
        #endif
        irule_rdsav++;
        if (char_skip == char_code) {
          blevel++;
        }
        else {
          pbracket = prepare_invert_bracket(char_skip);
          if (pbracket == char_code) {
            if (blevel == 0) {
              make_state = PREPARE_MODE_MAKE_COPYCODE;
              #ifdef __ALLOW_DEBUG_DUMP
                printf(" -> COPYCODE %c-%c-%c", char_skip, char_code, pbracket);
              #endif
            }
            else {
              blevel--;
            }
          }
        }
        #ifdef __ALLOW_DEBUG_DUMP
          printf("\r\n");
        #endif
      break;
    }
  }
  prepare_raise_error(pself, PREPARE_CODE_ERR_TIMEOUT);
  return (PREPARE_STATUS_ERR);
}

/*******************************************************************************
 * Funkce priradi do rules MACRO indexy mista, kde makro zacina.
 *
 */
prepare_status_e prepare_assign_macrobegins(prepare_main_t * pself)
{
  unsigned short imacro_act, icode_act, begin_act;
  prepare_data_macro_t * macro_ptr;
  char succes;

  #ifdef __ALLOW_DEBUG_DUMP
    printf("\r\nVyhledani zacatku maker. Pocet Maker:%d\r\n", pself->MacroCnt);
  #endif

  for (icode_act = 0; icode_act < pself->CodeCnt; icode_act++) {
    if (pself->CodeTab[icode_act].Symbol == PREPARE_SYMBOL_MACRO) {

      macro_ptr = (prepare_data_macro_t *)pself->CodeTab[icode_act].StructPtr;
      succes = 0;
      if (pself->MacroCnt > 0) {
        for (imacro_act = 0; imacro_act < pself->MacroCnt; imacro_act++) {
          if (pself->MacroTab[imacro_act].Id == macro_ptr->Id) {
            begin_act = pself->MacroTab[imacro_act].BeginIdx;
            succes = 1;
            break;
          }
        }
      }

      #ifdef __ALLOW_DEBUG_DUMP
        printf(" Macro %d - Id:%d Begin:%d\r\n", icode_act, macro_ptr->Id, begin_act);
      #endif

      if (succes == 0) {
        pself->RowPos = macro_ptr->SaveRow;
        pself->ColPos = macro_ptr->SaveCol;
        prepare_raise_error(pself, PREPARE_RULE_ERR_MACRO3);
        return (PREPARE_STATUS_ERR);
      }
      else {
        macro_ptr->BeginIdx = begin_act;
      }
    }
  }
  return (PREPARE_STATUS_OK);
}

/*******************************************************************************
 * Funkce zobrazi zatim pouze symboly transformovanych rules.
 *
 */

prepare_status_e prepare_display_codesymbols(prepare_main_t * pself)
{
  #ifdef __ALLOW_DEBUG_DUMP
    unsigned short i;

    printf("\r\nTotal rule primary symbols: %u\r\n", pself->RuleCnt);
    for (i = 0; i < pself->RuleCnt; i++) {
      printf("%c", pself->RuleTab[i].Symbol);
    }
    printf("\r\n------------------------------------------------------\r\n");

    for (i = 0; i < pself->RuleCnt; i++) {
      printf("%c %p \r\n", pself->RuleTab[i].Symbol, pself->RuleTab[i].StructPtr);
    }
    printf("\r\n------------------------------------------------------\r\n");

    printf("\r\nTotal rule symbols processed: %u\r\n", pself->CodeCnt);
    for (i = 0; i < pself->CodeCnt; i++) {
      printf("%c", pself->CodeTab[i].Symbol);
    }
    printf("\r\n------------------------------------------------------\r\n");

    for (i = 0; i < pself->CodeCnt; i++) {
      printf("%c %p \r\n", pself->CodeTab[i].Symbol, pself->CodeTab[i].StructPtr);
    }
    printf("\r\n------------------------------------------------------\r\n");

    for (i = 0; i < pself->CodeCnt; i++) {
      printf("%03u %c %03u \r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order);
    }
    printf("\r\n------------------------------------------------------\r\n");

    printf("\r\nTotal macro members processed: %u\r\n", pself->MacroCnt);
    for (i = 0; i < pself->MacroCnt; i++) {
      printf("%d Id:%d Begin:%d\r\n", i, pself->MacroTab[i].Id, pself->MacroTab[i].BeginIdx);
    }
    printf("\r\n------------------------------------------------------\r\n");
  #endif

  return (PREPARE_STATUS_OK);
}


/*******************************************************************************
 * Funkce zobrazi pridavne hodnoty u vsech rule.
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Predelat na union
 */

prepare_status_e prepare_display_codeparams(prepare_main_t * pself)
{
  #ifdef __ALLOW_DEBUG_DUMP
    unsigned short i;

    prepare_data_anyspace_t * anyspace_ptr;
    prepare_data_oneormorespace_t * oneormorespace_ptr;
    prepare_data_statictext_t * statictext_ptr;
    prepare_data_variabletext_t * variabletext_ptr;
    prepare_data_synctext_t * synctext_ptr;
    prepare_data_skiptext_t * skiptext_ptr;
    prepare_data_variablenumber_t * variablenumber_ptr;
    prepare_data_calltask_t * task_ptr;
    prepare_data_macro_t * macro_ptr;
    prepare_data_lbracket_t * lbracket_ptr;
    prepare_data_endblock_t * endblock_ptr;

    printf("\r\nTotal rules: %u\r\n", pself->RuleCnt);

    for (i = 0; i < pself->CodeCnt; i++) {
      switch (pself->CodeTab[i].Symbol) {

        case PREPARE_SYMBOL_ANYSPACE      :
          anyspace_ptr = (prepare_data_anyspace_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, anyspace_ptr->Reverser, anyspace_ptr->ChrScale);
        break;

        case PREPARE_SYMBOL_ONEORMORESPACE:
          oneormorespace_ptr = (prepare_data_oneormorespace_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, oneormorespace_ptr->Reverser, oneormorespace_ptr->ChrScale);
        break;

        case PREPARE_SYMBOL_STATICTEXT    :
          statictext_ptr = (prepare_data_statictext_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u, Rev:%c, Sca:--- , :Type=%c, Idx=%d, Text=%s\r\n",
          i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, statictext_ptr->Reverser, statictext_ptr->Type, statictext_ptr->TextIdx, (char *)(pself->DataTab + statictext_ptr->TextIdx));
        break;
        case PREPARE_SYMBOL_VARIABLETEXT  :
          variabletext_ptr = (prepare_data_variabletext_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u,  :Type=%c\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, variabletext_ptr->Reverser, variabletext_ptr->ChrScale, variabletext_ptr->Type);
        break;
        case PREPARE_SYMBOL_SYNCTEXT      :
          synctext_ptr = (prepare_data_synctext_t *)pself->CodeTab[i].StructPtr;
          if (synctext_ptr->Type == PREPARE_TXSTATIC_NORMAL) {
            printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u, :Idx=%d, Text=%s\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, synctext_ptr->Reverser, synctext_ptr->ChrScale, synctext_ptr->TextIdx, (char *)(pself->DataTab + synctext_ptr->TextIdx));
          }
          else if (synctext_ptr->Type == PREPARE_TXSTATIC_WHITE) {
            printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u, :Idx=%d, Whitechar=%d\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, synctext_ptr->Reverser, synctext_ptr->ChrScale, synctext_ptr->TextIdx, (int)*(pself->DataTab + synctext_ptr->TextIdx));
          }  
          else if (synctext_ptr->Type == PREPARE_TXSTATIC_ERROR) {
            printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u, :Idx=%d, ERROR\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, synctext_ptr->Reverser, synctext_ptr->ChrScale, synctext_ptr->TextIdx);
          }
        break;
        case PREPARE_SYMBOL_VARIABLENUMBER:
          variablenumber_ptr = (prepare_data_variablenumber_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u,  :Type=%c, Format=%c\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, variablenumber_ptr->Reverser, variablenumber_ptr->ChrScale, variablenumber_ptr->Type, variablenumber_ptr->Format);
        break;
        case PREPARE_SYMBOL_SKIPTEXT      :
          skiptext_ptr = (prepare_data_skiptext_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u, Rev:%c, Sca:%03u, Type:%c\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, skiptext_ptr->Reverser, skiptext_ptr->ChrScale, skiptext_ptr->Type);
        break;
        case PREPARE_SYMBOL_CALLTASK      :
          task_ptr = (prepare_data_calltask_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u,                  :Number=%d\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, task_ptr->Number);
        break;
        case PREPARE_SYMBOL_MACRO         :
          macro_ptr = (prepare_data_macro_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u,                  :Id=%d BeginIdx=%d\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, macro_ptr->Id, macro_ptr->BeginIdx);
        break;
        case PREPARE_SYMBOL_ENDBLOCK      :
          endblock_ptr = (prepare_data_endblock_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u,                  :RetIdx=%d\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, endblock_ptr->RetIdx);
        break;
        case PREPARE_SYMBOL_LBRACKET      :
        case PREPARE_SYMBOL_LOPTION       :
        case PREPARE_SYMBOL_LREPEAT       :
          lbracket_ptr = (prepare_data_lbracket_t *)pself->CodeTab[i].StructPtr;
          printf("%03d  %c :Ord:%03u,                  :RuleIdx=%d\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order, lbracket_ptr->BeginIdx);
        break;
        default:
          printf("%03d  %c :Ord:%03u\r\n", i, pself->CodeTab[i].Symbol, pself->CodeTab[i].Order);
        break;
      }
    }
  #endif
  return (PREPARE_STATUS_OK);
}

/********************************************************************/
/********************************************************************/